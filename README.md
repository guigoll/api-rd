Sistema de Reembolso de Despesas

Tecnologias adotadas:

Rails v5.2.3 
Ruby v2.63 
Banco de dados PostgreSQL

Principais Rotinas:

Relatório de despesas. 
Lançamento de Adiantamentos 
Fundo Fixo 
Integrações com ERP's 
Integração com o CORE do team ECRM