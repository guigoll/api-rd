class Empresa < ApplicationRecord
  belongs_to :pessoa
  belongs_to :tenant

  has_one :empresa_configuracao

  has_many :empresa_moedas
  has_many :moedas, through: :empresa_moedas 

  has_many :funcionario_empresas
  has_many :funcionarios, through: :funcionario_empresas

  validates_presence_of :matriz
end
