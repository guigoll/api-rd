class Estado < ApplicationRecord
  belongs_to :pai
  has_many :cidades

  validates_presence_of :descricao, :sigla, :pai_id
end
