class PoliticaDeDespesa < ApplicationRecord
  belongs_to :tenant

  has_many :despesas

  validates_presence_of  :descricao, :limitemaximodediaspassados, :tenant_id
end
