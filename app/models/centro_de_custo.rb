class CentroDeCusto < ApplicationRecord
  belongs_to :tenant

  has_many :despesas

  validates_presence_of  :descricao
end
