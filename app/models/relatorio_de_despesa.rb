class RelatorioDeDespesa < ApplicationRecord
  belongs_to :funcionario
  belongs_to :fluxo_de_aprovacao
  
  has_one :relatorio_despesa

  validates_presence_of :descricao, 
                        :funcionario_id, 
                        :fluxo_de_aprovacao_id

  accepts_nested_attributes_for :relatorio_despesa, allow_destroy: true
end
