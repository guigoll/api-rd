class Aprovador < ApplicationRecord
  belongs_to :funcionario
  belongs_to :fluxo_de_aprovacao

  validates_presence_of :condicao, :nivel, :grupo
end
