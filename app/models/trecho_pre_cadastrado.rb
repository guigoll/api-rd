class TrechoPreCadastrado < ApplicationRecord
  belongs_to :politica_de_quilometragem
  belongs_to :tenant 

  validates_presence_of :cidadeorigem, 
                        :cidadedestino,
                        :politica_de_quilometragem_id,
                        :tenant_id
end
