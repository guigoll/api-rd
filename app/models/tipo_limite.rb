class TipoLimite < ApplicationRecord
    has_many :tipo_de_despesas 

    validates_presence_of :descricao
end
