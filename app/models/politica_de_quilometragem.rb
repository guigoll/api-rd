class PoliticaDeQuilometragem < ApplicationRecord
  belongs_to :tenant 

  has_many :quilometragems

  validates_presence_of  :valorpagoporkm, 
                         :permitiralterarvalormanualmente,
                         :tenant_id
end
