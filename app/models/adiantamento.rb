class Adiantamento < ApplicationRecord 
  belongs_to :moeda
  belongs_to :tipo_de_adiantamento

  has_one :relatorio_despesa

  has_many :relatorio_de_despesas

  has_many :adiantamento_funcionarios
  has_many :funcionarios, through: :adiantamento_funcionarios

  validates_presence_of :valor, 
                        :finalidade, 
                        :datanecessidade, 
                        :funcionario_id,
                        :moeda_id,
                        :tipo_de_adiantamento_id
end
