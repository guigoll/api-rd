class TipoDeDespesa < ApplicationRecord
  belongs_to :tipo_limite
  belongs_to :tipo_limite_de_lancamento
  belongs_to :tenant

  has_many :despesas 

  validates_presence_of :descricao,
                        :tipo_limite_id,
                        :tipo_limite_de_lancamento_id ,                      
                        :tenant_id
end
