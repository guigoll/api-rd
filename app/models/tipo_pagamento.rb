class TipoPagamento < ApplicationRecord
  belongs_to :moeda
  belongs_to :tenant

  has_many :despesas 

 validates_presence_of  :descricao,                        
                        :moeda_id, 
                        :tenant_id
         
end
