class Funcionario < ApplicationRecord
  belongs_to :pessoa
  belongs_to :centro_de_custo
  belongs_to :departamento
  belongs_to :politica_de_despesa
  belongs_to :fluxo_de_aprovacao
  belongs_to :politica_de_quilometragem
   
  has_one :aprovador
  has_one :funcionario_cartao

  has_many :despesas
  has_many :relatorio_de_despesas
  
  has_many :adiantamento_funcionarios
  has_many :adiantamentos, through: :adiantamento_funcionarios

  has_many :funcionario_bancos
  has_many :bancos, through: :funcionario_bancos

  has_many :funcionario_empresas
  has_many :empresas, through: :funcionario_empresas

  has_many :projeto_funcionarios
  has_many :projetos, through: :projeto_funcionarios

  validates_presence_of :pessoa_id, 
                        :centro_de_custo_id, 
                        :departamento_id,
                        :politica_de_despesa_id,
                        :fluxo_de_aprovacao_id,
                        :politica_de_quilometragem_id

end
