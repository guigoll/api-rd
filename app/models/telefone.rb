class Telefone < ApplicationRecord
  belongs_to :pessoa

  validates_presence_of :numero, :pessoa_id
end
