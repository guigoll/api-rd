class Percurso < ApplicationRecord
  belongs_to :centro_de_custo
  belongs_to :tipo_pagamento
  belongs_to :trecho_pre_cadastrado

  has_one :relatorio_despesa

  validates_presence_of  :descricao, 
                         :reembolsavel,                        
                         :centro_de_custo_id,
                         :tipo_pagamento_id,
                         :trecho_pre_cadastrado_id
end
