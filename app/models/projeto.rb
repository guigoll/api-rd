class Projeto < ApplicationRecord
  belongs_to :cliente
  belongs_to :tenant

  has_many :projeto_funcionarios
  has_many :funcionarios, through: :projeto_funcionarios

  validates_presence_of :descricao, :cliente_id, :tenant_id

end
