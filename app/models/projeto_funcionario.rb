class ProjetoFuncionario < ApplicationRecord
  belongs_to :projeto
  belongs_to :funcionario

  validates_presence_of :projeto_id, :funcionario_id
end
