class FuncionarioEmpresa < ApplicationRecord
  belongs_to :funcionario
  belongs_to :empresa
  belongs_to :perfil

  validates_presence_of :funcionario_id, :empresa_id, :perfil_id
end
