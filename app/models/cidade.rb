class Cidade < ApplicationRecord
  belongs_to :estado
  
  has_many :enderecos

  validates_presence_of  :descricao
end
