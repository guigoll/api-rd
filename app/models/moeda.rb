class Moeda < ApplicationRecord
  belongs_to :tenant

  has_many :despesas
  has_many :empresa_moedas
  has_many :empresas, through: :empresa_moedas

  validates_presence_of :descricao, :tenant_id
end
