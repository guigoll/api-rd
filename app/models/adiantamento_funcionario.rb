class AdiantamentoFuncionario < ApplicationRecord
  belongs_to :funcionario
  belongs_to :adiantamento

  validates_presence_of :adiantamento_id, :funcionario_id

end
