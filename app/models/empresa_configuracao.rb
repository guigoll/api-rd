class EmpresaConfiguracao < ApplicationRecord
  belongs_to :empresa 

  validates_presence_of :permitiralteracaomembrosequipeapenasporadmin, 
                       :permitirenviodeemaildeaprovacaoderelatorio,
                       :descricaodespesaobrigatoria,
                       :tornacampoformadepagtoobrigatorio,
                       :permitirlancamentoscomvalornegativo,
                       :tornarcampoprojetodadespesaobrigatorio,
                       :permitirutiliaradiantamento
end
