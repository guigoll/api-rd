class Perfil < ApplicationRecord
    has_many :funcionario_empresas  
    
    validates_presence_of :descricao
end
