class Departamento < ApplicationRecord
  belongs_to :tenant

  validates_presence_of  :descricao, :tenant_id
end
