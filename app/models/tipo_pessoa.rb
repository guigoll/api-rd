class TipoPessoa < ApplicationRecord
    has_many :pessoas

    validates_presence_of :descricao
end
