class Pessoa < ApplicationRecord
  belongs_to :tipo_pessoa
  belongs_to :tenant

  has_one :cliente
  has_one :funcionario

  has_many :telefones
  has_many :enderecos

  validates_presence_of :tipo_pessoa_id, :ativo, :tenant_id

  accepts_nested_attributes_for :telefones, allow_destroy: true
  accepts_nested_attributes_for :enderecos, allow_destroy: true
  accepts_nested_attributes_for :funcionario, allow_destroy: true
  accepts_nested_attributes_for :cliente, allow_destroy: true

end
