class EmpresaMoeda < ApplicationRecord
  belongs_to :empresa
  belongs_to :moeda

  validates_presence_of :moeda_id, :empresa_id
end
