class TipoDeAdiantamento < ApplicationRecord
  belongs_to :tenant 

  validates_presence_of :descricao, :valorlimite, :fundofixo, :tenant_id
end
