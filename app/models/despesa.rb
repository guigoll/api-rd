class Despesa < ApplicationRecord
  belongs_to :moeda
  belongs_to :centro_de_custo
  belongs_to :tipo_pagamento
  belongs_to :tipo_de_despesa
  belongs_to :politica_de_despesa
  belongs_to :funcionario

  has_one :relatorio_despesa

  validates_presence_of  :descricao,
                         :reembolsavel, 
                         :moeda_id, 
                         :centro_de_custo_id, 
                         :tipo_pagamento_id,
                         :tipo_de_despesa_id,
                         :politica_de_despesa_id,
                         :funcionario_id
   
end
