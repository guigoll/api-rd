class RelatorioDespesa < ApplicationRecord
  belongs_to :despesa
  belongs_to :adiantamento
  belongs_to :relatorio_de_despesa
  belongs_to :percurso

  validates_presence_of :relatorio_de_despesa_id
end
