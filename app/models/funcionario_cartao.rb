class FuncionarioCartao < ApplicationRecord
  belongs_to :cartao_operadora
  belongs_to :funcionario

  validates_presence_of :numero, 
                        :digito, 
                        :ativo, 
                        :limite, 
                        :funcionario_id, 
                        :cartao_operadora_id
end
