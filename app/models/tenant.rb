class Tenant < ApplicationRecord
    has_many :pessoas
    has_many :departamentos
    has_many :centro_de_custos
    has_many :users
end
