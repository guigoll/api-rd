class Banco < ApplicationRecord 
    has_many :funcionario_bancos
    has_many :funcionarios, through: :funcionario_bancos

    validates_presence_of :descricao, :codigobc
end
