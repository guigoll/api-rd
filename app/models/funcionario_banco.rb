class FuncionarioBanco < ApplicationRecord
  belongs_to :funcionario
  belongs_to :banco

  validates_presence_of :funcionario_id, :banco_id
end
