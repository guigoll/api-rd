class Quilometragem < ApplicationRecord
  belongs_to :politica_de_quilometragem
  belongs_to :tenant

  validates_presence_of :descricao, 
                        :valorpagoporkm, 
                        :permitiralterarvalormanualmente, 
                        :exigirimagem,
                        :tenant_id
end
