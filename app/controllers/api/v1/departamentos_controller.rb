class Api::V1::DepartamentosController < ApplicationController     

    def index
        departamento= Departamento.all
        render json: {departamento: departamento}, status: 200    
    end

    def show
       departamento = Departamento.find(params[:id])
       render json: departamento, status: 200
    end

    def create
        departamento = Departamento.create(departamento_params)
        # byebug
        if departamento.save
            render json: departamento, status: 201       
        else
            render json: {errors: departamento.errors}, status: 422
        end
    end

    def update
        departamento = Departamento.find(params[:id])

        if departamento.update_attributes(departamento_params)
            render json: departamento, status: 200
        else
            render json: {errors: departamento.errors}, status: 422
        end    
    end

    def destroy
      departamento = Departamento.find(params[:id])
    # byebug
      departamento.destroy 
      render json: departamento, status: 200   
    end

    private

    def departamento_params
        params.require(:departamento).permit(:descricao, :tenant_id)
    end    

end
