class Api::V1::TenantController < ApplicationController

    def index
       tenant = Tenant.all 
       render json: {tenant: tenant}, status: 200
    end

    def show
       tenant = Tenant.find(params[:id])
       render json: tenant, status: 200
    end

    def create
       tenant = Tenant.create(tenant_params)
       if tenant.save
          render json: tenant, status: 201
       else
          render json:{errors: tenant}, status: 422
       end
    end

    def update
       tenant = Tenant.find(params[:id])
       #byebug
       if tenant.update_attributes(tenant_params)
          render json: tenant, status: 200
       else
          render json: {errors: tenant}, status: 422
       end
    end

    private
    def tenant_params
      params.require(:tenant).permit(:codigocliente)
    end
end
