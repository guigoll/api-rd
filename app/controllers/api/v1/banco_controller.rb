class Api::V1::BancoController < ApplicationController

    def index
        banco = Banco.all
        render json: {banco: banco}, status: 200
    end

    def show
        banco = Banco.find(params[:id])
        render json: banco, status: 200
    end

    def create
        banco = Banco.create(banco_params)
        # byebug
        if banco.save        
           render json: banco, status: 201
        else
        render json: {errors: banco}, status: 422
        end
    end

    def update
        banco = Banco.find(params[:id])
        if banco.update_attributes(banco_params)
            render json: banco, status: 200
        else 
            render json: {errors: banco}, status: 422
        end        
    end
    
    def destroy
        banco = Banco.find(params[:id])
        banco.destroy
        render json: banco, status: 200
    end
    
    
    private
    def banco_params
        params.require(:banco).permit(:descricao, :codigobc)
    end
    
end
