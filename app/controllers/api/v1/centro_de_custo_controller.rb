class Api::V1::CentroDeCustoController < ApplicationController

    def index
        centro = CentroDeCusto.all
        render json: {centro: centro}, status: 200
    end

    def show
      centro = CentroDeCusto.find(params[:id])
      render json: centro, status: 200
    end

    def create
       centro = CentroDeCusto.create(centro_params)
       if centro.save 
        render json: centro, status: 201
       else
        render json: {errors: centro}, status: 422
       end
    end

    def update
      centro = CentroDeCusto.find(params[:id])
      if centro.update_attributes(centro_params)
         render json: centro, status: 200
      else
         render json:{errors: centro}, status: 422
      end
    end

    def destroy
        centro = CentroDeCusto.find(params[:id])
        centro.destroy
        render json: centro, status: 200    
    end

    private
    def centro_params
     params.require(:centro).permit(:descricao, :tenant_id)
    end

end
