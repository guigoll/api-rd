class Api::V1::CartaoOperadoraController < ApplicationController

    def index
       operadora = CartaoOperadora.all
       render json: {operadora: operadora}, status: 200
    end

    def show
        operadora = CartaoOperadora.find(params[:id])
        render json: operadora
    end

    def create
       operadora = CartaoOperadora.create(operadora_params)
       if operadora.save
           render json: operadora, status:201
       else 
          render json:{errors: operadora}, status: 422
       end
    end

    def update
        operadora = CartaoOperadora.find(params[:id])
        if operdora.update_attributes(operadora)
            render json: operadora, status: 200
        else
           render json:{errors: operadora}, status: 422
        end
    end
    

    private
    def operadora_params
       params.require(:operadora).permit(:descricao)
    end
    
end
