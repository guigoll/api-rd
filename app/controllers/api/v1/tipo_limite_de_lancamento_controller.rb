class Api::V1::TipoLimiteDeLancamentoController < ApplicationController

    def index
      limite_lancamento = TipoLimiteDeLancamento.all
      render json:{limite_lancamento: limite_lancamento}, status: 200
    end

    def show
        limite_lancamento = TipoLimiteDeLancamento.find(params[:id])  
        render json: limite_lancamento, status: 200
    end
    
end
