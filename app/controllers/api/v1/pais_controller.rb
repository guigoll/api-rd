class Api::V1::PaisController < ApplicationController
    
    def index
        pais = Pai.all 
        render json:{pais: pais}, status: 200
    end

    def show
        pais = Pai.find(params[:id])
        render json: pais, status: 200
    end    

end
