class Api::V1::TipoPagamentoController < ApplicationController

    def index
        tipo = TipoPagamento.all
         render json: {tipo: tipo}, status: 200    
    end

    def show
       tipo = TipoPagamento.find(params[:id])
       render json: tipo, status:  200
    end

    def create
        tipo = TipoPagamento.create(tipo_params)
        if tipo.save
           render json: tipo, status: 201
        else
           render json: {errors: tipo}, status: 422
        end
    end

    def update
       tipo = TipoPagamento.find(params[:id])
       if tipo.update_attributes(tipo_params)
         render json: tipo, status: 200
       else
         render json:{errors: tipo}, status: 422
       end
    end

    def destroy
        tipo = TipoPagamento.find(params[:id])
        tipo.destroy
        render json: tipo, status: 200
    end
    

    private
    def tipo_params
       params.require(:tipo).permit(:descricao, 
                                    :reembolsavel, 
                                    :baixardoadiantamento,
                                    :moeda_id,
                                    :tenant_id)
    end
    
end
