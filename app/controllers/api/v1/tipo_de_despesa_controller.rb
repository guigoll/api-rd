class Api::V1::TipoDeDespesaController < ApplicationController

    def index
        tipo = TipoDeDespesa.all
        render json: {tipo: tipo}, status: 200
    end

    def show
        tipo =  TipoDeDespesa.find(params[:id])
        render json: tipo, status:200
    end

    def create
        tipo = TipoDeDespesa.create(tipo_params)   
        # byebug     
        if tipo.save
          render json: tipo, status: 201
        else
          render json: {errors: tipo}, status: 422
        end
    end

    def update
      tipo = TipoDeDespesa.find(params[:id])       
      if tipo.update_attributes(tipo_params)  
         render json: tipo, status: 200        
      else        
        render json: {errors: tipo}, status: 422
      end
    end
    
    private
    def tipo_params
      params.require(:tipo).permit(:descricao, 
                                   :permitirultrapassarlimite, 
                                   :observacaoobrigatorio, 
                                   :exigerecibo,
                                   :tenant_id,
                                   :tipo_limite_id,
                                   :tipo_limite_de_lancamento_id)
    end
end
