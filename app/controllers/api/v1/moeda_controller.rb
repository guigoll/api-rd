class Api::V1::MoedaController < ApplicationController

    def index
       moeda = Moeda.all
       render json: {moeda: moeda}, status: 200    
    end

    def show
        moeda = Moeda.find(params[:id])
        render json: moeda, status: 200
    end

    def create
        moeda = Moeda.create(moeda_params)
        if moeda.save         
           render json: moeda, status: 201
        else
          render json: {errors: moeda}, status: 422
        end
    end

    def update
        moeda = Moeda.find(params[:id])
        if moeda.update_attributes(moeda_params)       
            render json: moeda, status: 200
        else
            render json: {errors: moeda}, status: 422
        end
    end

    def destroy
        moeda =  Moeda.find(params[:id])
        moeda.destroy
        render json: moeda, status: 200
    end

    private 
    def moeda_params
       params.require(:moeda).permit(:descricao, :tenant_id)
    end
end
