class Api::V1::TipoLimiteController < ApplicationController

    def index
      tipo_limite =  TipoLimite.all
      render json:{ tipo_limite: tipo_limite}, status: 200
    end

    def show
      tipo_limite = TipoLimite.find(params[:id])
      render json: tipo_limite, status: 200
    end
    
end
