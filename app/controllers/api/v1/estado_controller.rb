class Api::V1::EstadoController < ApplicationController

    def index
        estado =  Estado.all
        render json: {estado: estado}, status: 200    
    end

    def show
       estado = Estado.find(params[:id])
       render json: estado, status: 200
    end
end
