class Api::V1::CidadeController < ApplicationController

    def index
      cidade = Cidade.all 
      render json: { cidade: cidade}, status: 200
    end

   def show
       cidade = Cidade.find(params[:id])
       render json: cidade, status: 200
   end

end
