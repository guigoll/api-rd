class ApplicationController < ActionController::API
    include DeviseTokenAuth::Concerns::SetUserByToken

    before_action :configure_permitted_parameters, if: :devise_controller?

    protected

  def configure_permitted_parameters
    atrr_after = [:name, :tenant_id]
    atrr_before = [:name,:tenant_id]

    devise_parameter_sanitizer.permit(:sign_up, keys: atrr_after)
    devise_parameter_sanitizer.permit(:sign_in, keys: atrr_before)
    devise_parameter_sanitizer.permit(:account_update, keys: atrr_before)
  end

end
  