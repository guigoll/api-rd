require 'api_version_constraint'

Rails.application.routes.draw do

  # Mapear as sessões que foram solicitadas
  devise_for :users, only: [:sessions], controller: {sessions: 'api/v1/sessions'}

   namespace :api, default:{ format: :json}, path: '/' do
       namespace :v1, path: '/', constraints: ApiVersionConstraint.new(version: 1, default: true) do
         mount_devise_token_auth_for 'User', at: 'auth'   

         resources :users, only:[:index, :show, :create] 

         # Rotas que podem ser influenciadas pelo usuario 
         resources :departamentos, only: [:index, :show, :create, :update, :destroy]  
         resources :centro_de_custo, only: [:index, :show, :create, :update, :destroy]   
         resources :moeda, only: [:index, :show, :create, :update, :destroy]
         resources :banco, only: [:index, :show, :create, :update, :destroy]
         resources :tipo_pagamento, only: [:index, :show, :create, :update, :destroy]
         resources :tenant, only: [:index, :show, :create, :update]
         resources :tipo_de_despesa, only: [:index, :show, :create, :update]

         # Rotas de sistemas
         resources :pais, only: [:index, :show]
         resources :estado, only: [:index, :show]
         resources :cidade, only: [:index, :show]
         resources :tipo_limite, only: [:index, :show]
         resources :tipo_limite_de_lancamento, only: [:index, :show]
       end

   end
end
