require 'rails_helper'

RSpec.describe 'Tipo Pagamento API', type: :request do
  before { host! 'api.permata.test'}

  let(:tenant){ FactoryBot.create(:tenant)}
  let(:moeda){ FactoryBot.create(:moeda)}
  let(:user){ FactoryBot.create(:user)}
  let(:auth_data){ user.create_new_auth_token }


  let(:headers) do
    {
      'Accept' => 'application/vnd.permata.v1',
      'Content-type' => Mime[:json].to_s,
      'Access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end
  
  describe 'GET / TipoPagamento' do
      before do
         create_list(:tipo_pagamento, 5)
         get '/tipo_pagamento', params: {}, headers: headers
      end

      it 'retornar status code 200' do
          expect(response).to have_http_status(200)  
      end

     it 'retornar o 5 tipo de pagamento' do
         expect(json_body[:tipo].count).to eq(5)
     end          
  end

describe 'GET /TipoPagamento/:id' do
    let(:tipo_pagamento){ create(:tipo_pagamento)}

    before{ get "/tipo_pagamento/#{tipo_pagamento.id}", params: {}, headers: headers}

    it 'retornar status code 200' do
        expect(response).to have_http_status(200)
    end

    it 'retornar o json do banco' do
        expect(json_body[:descricao]).to include(tipo_pagamento.descricao)
    end       
end

describe 'POST /TipoPagamento' do
     before do
         post '/tipo_pagamento', params:{tipo: tipo_params }.to_json, headers: headers
     end

     context "Quando os parametros são validos" do
          let(:tipo_params){ FactoryBot.build(:tipo_pagamento, tenant_id: tenant.id, moeda_id: moeda.id)}

          it "retorna status code 201" do
            expect(response).to have_http_status(201)
          end      
          
          it "salvar no banco de dados" do
              expect(TipoPagamento.find_by(descricao: tipo_params[:descricao])).not_to be_nil
          end

          it "retornar json quando criado no banco" do
              expect(json_body[:descricao]).to include(tipo_params[:descricao])
          end                  
     end

     context "Quando a descricao é invalida" do
          let(:tipo_params){ FactoryBot.build(:tipo_pagamento, descricao: '')}

          it "retornar status code 422" do
              expect(response).to have_http_status(422)
          end

          it "quando a descricao não é informada não salva no banco" do
              expect(TipoPagamento.find_by(descricao: tipo_params[:descricao])).to be_nil
          end
          
          it "retorna o json de erro" do
              expect(json_body).to have_key(:errors)
          end          
     end
     
     context "Quando não é informa uma moeda para o tipo de pagamento" do
         let(:tipo_params){ FactoryBot.build(:tipo_pagamento, moeda_id: 0)}

         it "retorna status code 422" do
             expect(response).to have_http_status(422)
         end

         it "quando não informado a FK moeda_id não e salva no banco o cadastro" do
             expect(TipoPagamento.find_by(moeda_id: tipo_params[:moeda_id])).to be_nil
         end
         
         it "retorna json de erro quando não informado a moeda" do
             expect(json_body).to have_key(:errors)
         end                 
     end          
end

 describe 'PUT /TipoPagamento/:id' do
     let!(:tipo_pagamento){create(:tipo_pagamento)}

     before do
        put "/tipo_pagamento/#{tipo_pagamento.id}", params: {tipo: tipo_params}.to_json, headers: headers
     end

     context "Quando os parametros são validos" do
        let(:tipo_params){{descricao: 'Teste de atualização'}}

        it "retorna status code 201" do
          expect(response).to have_http_status(200)
        end      
        
        it "salvar no banco de dados" do
            expect(TipoPagamento.find_by(descricao: tipo_params[:descricao])).not_to be_nil
        end

        it "retornar json quando criado no banco" do
            expect(json_body[:descricao]).to include(tipo_params[:descricao])
        end                  
   end

   context "Quando a descricao é invalida" do
        let(:tipo_params){{ descricao: ''}}

        it "retornar status code 422" do
            expect(response).to have_http_status(422)
        end

        it "quando a descricao não é informada não salva no banco" do
            expect(TipoPagamento.find_by(descricao: tipo_params[:descricao])).to be_nil
        end
        
        it "retorna o json de erro" do
            expect(json_body).to have_key(:errors)
        end          
   end
   
   context "Quando não é informa uma moeda para o tipo de pagamento" do
       let(:tipo_params){{ moeda_id: 0 }}

       it "retorna status code 422" do
           expect(response).to have_http_status(422)
       end

       it "quando não informado a FK moeda_id não e salva no banco o cadastro" do
           expect(TipoPagamento.find_by(moeda_id: tipo_params[:moeda_id])).to be_nil
       end
       
       it "retorna json de erro quando não informado a moeda" do
           expect(json_body).to have_key(:errors)
       end       
    end
end
 
describe 'DELETE /TipoPagamento/:id' do
     let(:tipo_pagamento){ create(:tipo_pagamento)}

     before do
       delete "/tipo_pagamento/#{tipo_pagamento.id}", params:{}, headers: headers
     end
     
     it "retorna status code 200" do
         expect(response).to have_http_status(200)
     end
     
     it "remover tipo de pagamento da base de dados" do
         expect(TipoPagamento.find_by(id: tipo_pagamento.id)).to be_nil
     end     
 end
end
