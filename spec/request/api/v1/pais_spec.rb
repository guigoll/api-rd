require 'rails_helper'

RSpec.describe 'Pais API', type: :request do
    before { host! 'api.permata.test'}

    let(:user){ FactoryBot.create(:user)}
    let(:auth_data){ user.create_new_auth_token }  
  
    let(:headers) do
      {
        'Accept' => 'application/vnd.permata.v1',
        'Content-type' => Mime[:json].to_s,
        'Access-token' => auth_data['access-token'],
        'uid' => auth_data['uid'],
        'client' => auth_data['client']
      }
    end
 
    describe 'GET /Pais' do
         before do
            create_list(:pai, 5)
            get '/pais', params: {}, headers: headers
         end

         it "retornar status code 200" do
              expect(response).to have_http_status(200)
         end         

         it "retornar 5 paises" do
             expect(json_body[:pais].count).to eq(5)
         end         
    end

    describe 'GET /pais/:id' do
      let(:pais){ create(:pai)}

      before{ get "/pais/#{pais.id}", params:{}, headers: headers}

      it "retornar status code 200" do
          expect(response).to have_http_status(200)
      end

      it "retorna o json do descricao" do
          expect(json_body[:descricao]).to eq(pais.descricao)
      end
   end   

end
