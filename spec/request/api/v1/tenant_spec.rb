require 'rails_helper'

RSpec.describe " Tenant API", type: :request do
    before { host! 'api.permata.test'}

  let(:headers) do
    {
      'Accept' => 'application/vnd.permata.v1',
      'Content-type' => Mime[:json].to_s     
    }
  end

  describe "GET /Tenant" do
      before do
        create_list(:tenant, 5)
        get '/tenant', params: {}, headers: headers
      end

      it "retornar status code 200" do
          expect(response).to have_http_status(200)
      end

      it "retorn 5 tenant criado" do
          expect(json_body[:tenant].count).to eq(5)
      end           
  end

  describe "GET /Tenant/:id" do
    let(:tenant){ create(:tenant)}

    before{ get "/tenant/#{tenant.id}", params:{}, headers: headers }

    it "retorna status code 200" do
        expect(response).to have_http_status(200)
    end

    it "retorna o json do tenant" do
        expect(json_body[:id]).to eq(tenant.id)
    end     
  end
  
  describe "POST /Tenant" do
      before do
         post '/tenant', params: {tenant: tenant_params}.to_json, headers: headers
      end

      context "Quando os parametros são validos" do
           let(:tenant_params){ create(:tenant)}
           
           it "retornar status code 201" do
               expect(response).to have_http_status(201)
           end           
      end      
  end
  
  describe "PUT /Tenant/:id" do
      let!(:tenant){ FactoryBot.create(:tenant)}

      before do
        put "/tenant/#{tenant.id}", params:{tenant: tenant_params}.to_json, headers: headers
      end

      context "Quando os parametros sao validos" do
          let(:tenant_params){{ codigocliente: 10}}
          it "retorna status code 200" do
              expect(response).to have_http_status(200)
          end          
      end      
  end   
end
