require 'rails_helper'

RSpec.describe 'Banco API', type: :request do
  before { host! 'api.permata.test'}

  let(:tenant){ FactoryBot.create(:tenant)}
  let(:user){ FactoryBot.create(:user)}
  let(:auth_data){ user.create_new_auth_token }


  let(:headers) do
    {
      'Accept' => 'application/vnd.permata.v1',
      'Content-type' => Mime[:json].to_s,
      'Access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  describe 'GET /Banco' do
    before do
      create_list(:banco, 5)
      get '/banco', params: {}, headers: headers
    end

    it 'retornar status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'retornar 5 bancos da base' do
      expect(json_body[:banco].count).to eq(5)
    end

  end

  describe 'GET /banco/:id' do
    let(:banco){ create(:banco) }

    before{ get "/banco/#{banco.id}", params: {}, headers: headers }

    it 'retornar status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'retorna o json do banco' do
      expect(json_body[:descricao]).to eq(banco.descricao)
    end

    it 'retorna o json do codigo BC' do
        expect(json_body[:codigobc]).to eq(banco.codigobc)
      end
  end

  describe 'POST /banco' do
    before do
      post '/banco', params: {banco: banco_params}.to_json, headers: headers
    end

    context 'Quando os parametros forem validos' do
      let(:banco_params){ FactoryBot.build(:banco, codigobc: 10)}

      it 'retornar status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'salvar banco na base de dados' do
        expect(Banco.find_by(descricao: banco_params[:descricao])).not_to be_nil        
      end

      it 'retornar json quando criado o banco' do
        expect(json_body[:descricao]).to eq(banco_params[:descricao])        
      end
    end

    context 'Quando os parametros não são validos' do
      let(:banco_params){ attributes_for(:banco, descricao: '')}

      it 'retornar status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'quando não salvar o banco no banco de dados' do
        expect(Banco.find_by(descricao: banco_params[:descricao])).to be_nil
      end

      it 'retornar json de erro' do
         expect(json_body).to have_key(:errors)
      end
    end
  end

  describe 'PUT /banco/:id' do
    let!(:banco){ create(:banco)}
    before do
      put "/banco/#{banco.id}", params: {banco: banco_params}.to_json, headers: headers
    end

    context 'quando os parametros são válidos' do
      let(:banco_params){{ descricao: 'Teste de atualização' }}

      it 'retornar  status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returnar json do banco atualizado' do
        expect(json_body[:descricao]).to eq(banco_params[:descricao])
      end

      it 'atualizado banco na base' do
        expect(Banco.find_by(descricao: banco_params[:descricao])).not_to be_nil
      end

    end

    context 'Quando os parametros são inválidos' do
      let(:banco_params){{ descricao: '' }}

      it 'retornar status code 422' do
        expect(response).to have_http_status(422)
      end   

      it 'não atualizadona base de dados o banco' do
        expect(Banco.find_by(descricao: banco_params[:descricao])).to be_nil
      end

      it 'retornar json de erro no campo descrição' do
        expect(json_body).to have_key(:errors)
      end
    end
  end
  
  describe 'DELETE /Banco/:id' do
    let!(:banco){ create(:banco)}
    before do
      delete "/banco/#{banco.id}", params: {}, headers: headers
    end

    it 'retornar status code 200' do
      expect(response).to have_http_status(200)
    end

   it 'remover banco da base de dados' do
      expect(Banco.find_by(id: banco.id)).to be_nil
   end

 end


end
