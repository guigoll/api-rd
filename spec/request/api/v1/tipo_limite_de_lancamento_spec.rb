require 'rails_helper'

RSpec.describe "Tipo Limite de Lancamento API", type: :request do
    before { host! 'api.permata.test'}

    let(:user){ FactoryBot.create(:user)}
    let(:auth_data){ user.create_new_auth_token }  
  
    let(:headers) do
      {
        'Accept' => 'application/vnd.permata.v1',
        'Content-type' => Mime[:json].to_s,
        'Access-token' => auth_data['access-token'],
        'uid' => auth_data['uid'],
        'client' => auth_data['client']
      }
    end

    describe 'GET /tipo_limite_de_lancamento' do
        before do
           create_list(:tipo_limite_de_lancamento, 5)
           get '/tipo_limite_de_lancamento', params: {}, headers: headers
        end

        it "retornar status code 200" do
             expect(response).to have_http_status(200)
        end         

        it "retornar 5 tipo de limite de lançamento" do
            expect(json_body[:limite_lancamento].count).to eq(5)
        end         
   end

   describe 'GET /tipo_limite_de_lancamento/:id' do
     let(:limite_lancamento){ create(:tipo_limite_de_lancamento)}

     before{ get "/tipo_limite_de_lancamento/#{limite_lancamento.id}", params:{}, headers: headers}

     it "retornar status code 200" do
         expect(response).to have_http_status(200)
     end

     it "retorna o json do descricao" do
         expect(json_body[:descricao]).to eq(limite_lancamento.descricao)
     end
  end   

end
