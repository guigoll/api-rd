require 'rails_helper'

RSpec.describe 'Tipo de Despesa API', type: :request do
  before { host! 'api.permata.test'}

  let!(:tenant){ FactoryBot.create(:tenant)}
  let!(:tipo_limite){ FactoryBot.create(:tipo_limite)}
  let!(:tipo_limite_de_lancamento){ FactoryBot.create(:tipo_limite_de_lancamento)}
  let!(:user){ FactoryBot.create(:user)}
  let(:auth_data){ user.create_new_auth_token }


  let(:headers) do
    {
      'Accept' => 'application/vnd.permata.v1',
      'Content-type' => Mime[:json].to_s,
      'Access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  describe 'GET /tipo_de_despesa' do
    before do
      create_list(:tipo_de_despesa, 5)
      get '/tipo_de_despesa', params: {}, headers: headers
    end

    it 'retornar status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'retornar 5 bancos da base' do
      expect(json_body[:tipo].count).to eq(5)
    end

  end

  describe 'GET /tipo_de_despesa/:id' do
    let(:tipo_de_despesa){ create(:tipo_de_despesa) }

    before{ get "/tipo_de_despesa/#{tipo_de_despesa.id}", params: {}, headers: headers }

    it 'retornar status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'retorna o json do tipo de despesa' do
      expect(json_body[:descricao]).to eq(tipo_de_despesa.descricao)
    end
  end

  describe 'POST /tipo_de_despesa' do
    before do
      post '/tipo_de_despesa', params: {tipo: tipo_params}.to_json, headers: headers
    end

    context 'Quando os parametros forem validos' do
      let!(:tipo_params){ FactoryBot.build(:tipo_de_despesa)}                                      
                                                                              
      it 'retornar status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'salvar banco na base de dados' do
        expect(TipoDeDespesa.find_by(descricao: tipo_params[:descricao])).not_to be_nil        
      end

      it 'retornar json quando criado o tipo de despesa' do
        expect(json_body[:descricao]).to eq(tipo_params[:descricao])        
      end
    end

    context 'Quando os parametros não são validos' do
      let(:tipo_params){ attributes_for(:tipo_de_despesa, descricao: '')}

      it 'retornar status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'quando não salvar o tipo de despesa no banco de dados' do
        expect(TipoDeDespesa.find_by(descricao: tipo_params[:descricao])).to be_nil
      end

      it 'retornar json de erro' do
         expect(json_body).to have_key(:errors)
      end
    end
  end

  describe 'PUT /tipo_de_despesa/:id' do
    let!(:tipo_de_despesa){ FactoryBot.create(:tipo_de_despesa, tenant_id: tenant.id,  tipo_limite_id: tipo_limite.id,  tipo_limite_de_lancamento_id: tipo_limite_de_lancamento.id)}  
    
    before do
      put "/tipo_de_despesa/#{tipo_de_despesa.id}", params: {tipo: tipo_params}.to_json, headers: headers
    end

    context 'quando os parametros são válidos' do
      let(:tipo_params){{ descricao: 'Teste de atualização' }}

      it 'retornar  status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returnar json do banco atualizado' do
        expect(json_body[:descricao]).to eq(tipo_params[:descricao])
      end

      it 'atualizado banco na base' do
        expect(TipoDeDespesa.find_by(descricao: tipo_params[:descricao])).not_to be_nil
      end

    end

    context 'Quando os parametros são inválidos' do
      let(:tipo_params){{ descricao: '' }}

      it 'retornar status code 422' do
        expect(response).to have_http_status(422)
      end   

      it 'não atualizadona base de dados o tipo de despesa' do
        expect(TipoDeDespesa.find_by(descricao: tipo_params[:descricao])).to be_nil
      end

      it 'retornar json de erro no campo descrição' do
        expect(json_body).to have_key(:errors)
      end
    end
  end
end
