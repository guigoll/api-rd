require 'rails_helper'

RSpec.describe 'Cidade API', type: :request do
    before { host! 'api.permata.test'}

    let(:user){ FactoryBot.create(:user)}
    let(:auth_data){ user.create_new_auth_token }  
  
    let(:headers) do
      {
        'Accept' => 'application/vnd.permata.v1',
        'Content-type' => Mime[:json].to_s,
        'Access-token' => auth_data['access-token'],
        'uid' => auth_data['uid'],
        'client' => auth_data['client']
      }
    end
 
    describe 'GET /Cidade' do
         before do
            create_list(:cidade, 5)
            get '/cidade', params: {}, headers: headers
         end

         it "retornar status code 200" do
              expect(response).to have_http_status(200)
         end         

         it "retornar 5 paises" do
             expect(json_body[:cidade].count).to eq(5)
         end         
    end

    describe 'GET /Cidade/:id' do
      let(:cidade){ create(:cidade)}

      before{ get "/cidade/#{cidade.id}", params:{}, headers: headers}

      it "retornar status code 200" do
          expect(response).to have_http_status(200)
      end

      it "retorna o json do descricao" do
          expect(json_body[:descricao]).to eq(cidade.descricao)
      end
   end   

end
