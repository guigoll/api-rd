require 'rails_helper'

RSpec.describe 'Moeda API', type: :request do
  before { host! 'api.permata.test'}

  let(:tenant){ FactoryBot.create(:tenant)}
  let(:user){ FactoryBot.create(:user)}
  let(:auth_data){ user.create_new_auth_token }


  let(:headers) do
    {
      'Accept' => 'application/vnd.permata.v1',
      'Content-type' => Mime[:json].to_s,
      'Access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  describe 'GET /Moeda' do
    before do
      create_list(:moeda, 5)
      get '/moeda', params: {}, headers: headers
    end

    it 'retornar status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'retornar 5 moedas da base' do
      expect(json_body[:moeda].count).to eq(5)
    end

  end

  describe 'GET /moedas/:id' do
    let(:moeda){ create(:moeda) }

    before{ get "/moeda/#{moeda.id}", params: {}, headers: headers }

    it 'retornar status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'retorna o json do moeda' do
      expect(json_body[:descricao]).to eq(moeda.descricao)
    end
  end

  describe 'POST /Moedas' do
    before do
      post '/moeda', params: {moeda: moeda_params}.to_json, headers: headers
    end

    context 'Quando os parametros forem validos' do
      let(:moeda_params){ FactoryBot.build(:moeda,tenant_id: tenant.id)}

      it 'retornar status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'salvar moeda no banco' do
        expect(Moeda.find_by(descricao: moeda_params[:descricao])).not_to be_nil
      end

      it 'retornar json quando criado a moeda' do
        expect(json_body[:descricao]).to eq(moeda_params[:descricao])
      end
    end

    context 'Quando os parametros não são validos' do
      let(:moeda_params){ attributes_for(:moeda, descricao: '')}

      it 'retornar status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'quando não salvar o departamento no banco' do
        expect(Moeda.find_by(descricao: moeda_params[:descricao])).to be_nil
      end

      it 'retornar json de erro' do
         expect(json_body).to have_key(:errors)
      end
    end
  end

  describe 'PUT /Moeda/:id' do
    let!(:moeda){ create(:moeda)}
    before do
      put "/moeda/#{moeda.id}", params: {moeda: moeda_params}.to_json, headers: headers
    end

    context 'quando os parametros são inválidos' do
      let(:moeda_params){{ descricao: 'Teste de atualização' }}

      it 'retornar  status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returnar json de moeda' do
        expect(json_body[:descricao]).to eq(moeda_params[:descricao])
      end

      it 'atualizado moeda na base' do
        expect(Moeda.find_by(descricao: moeda_params[:descricao])).not_to be_nil
      end

    end

    context 'Quando os parametros são inválidos' do
      let(:moeda_params){{ descricao: '' }}

      it 'retornar status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'retornar json de erro no campo descrição' do
           expect(json_body).to have_key(:errors)
      end

      it 'não atualizadona base de dados a moeda' do
        expect(Moeda.find_by(descricao: moeda_params[:descricao])).to be_nil
      end
    end
  end
  
  describe 'DELETE /Moeda/:id' do
    let!(:moeda){ create(:moeda)}
    before do
      delete "/moeda/#{moeda.id}", params: {}, headers: headers
    end

    it 'retornar status code 200' do
      expect(response).to have_http_status(200)
    end

   it 'remover moeda do banco' do
      expect(Moeda.find_by(id: moeda.id)).to be_nil
   end

 end


end
