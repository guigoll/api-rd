require 'rails_helper'

RSpec.describe 'Centro de Custo API', type: :request do
  before { host! 'api.permata.test'}

  let(:tenant){ FactoryBot.create(:tenant)}
  let(:user){ FactoryBot.create(:user)}
  let(:auth_data){ user.create_new_auth_token }


  let(:headers) do
    {
      'Accept' => 'application/vnd.permata.v1',
      'Content-type' => Mime[:json].to_s,
      'Access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  describe 'GET /CentroDeCusto' do
      before do
        create_list(:centro_de_custo, 5)
        get '/centro_de_custo', params: {}, headers: headers
      end
  
      it 'retornar status code 200' do
        expect(response).to have_http_status(200)
      end
  
      it 'retornar 5 centro de custo da base' do
        expect(json_body[:centro].count).to eq(5)
      end    
    end

    describe 'GET /CentroDeCusto/:id' do
      let(:centro_de_custo){ create(:centro_de_custo)}
      
      before { get "/centro_de_custo/#{centro_de_custo.id}", params:{}, headers: headers}

      it 'retorna status code 200' do
          expect(response).to have_http_status(200)
      end

      it 'retorna o json do centro de custo' do
          expect(json_body[:descricao]).to eq(centro_de_custo.descricao)
      end
    end

    describe 'POST /CentroDeCusto/:id' do
      before do
          post '/centro_de_custo', params: {centro: centro_params}.to_json, headers: headers
      end

      context 'Quando os parametros forem validos' do
        let(:centro_params){ FactoryBot.build(:centro_de_custo,tenant_id: tenant.id)}
  
        it 'retornar status code 201' do
          expect(response).to have_http_status(201)
        end
  
        it 'salvar dcentrod e custo no banco' do
          expect(CentroDeCusto.find_by(descricao: centro_params[:descricao])).not_to be_nil
        end
  
        it 'retornar json quando criado o departamento' do
          expect(json_body[:descricao]).to eq(centro_params[:descricao])
        end
      end
  
      context 'Quando os parametros não são validos' do
        let(:centro_params){ attributes_for(:centro_de_custo, descricao: '')}
  
        it 'retornar status code 422' do
          expect(response).to have_http_status(422)
        end
  
        it 'quando não salvar o centro de custo no banco' do
          expect(CentroDeCusto.find_by(descricao: centro_params[:descricao])).to be_nil
        end
  
        it 'retornar json de erro' do
           expect(json_body).to have_key(:errors)
        end
      end

    end    
  
    describe 'PUT /CentroDeCusto/:id' do
      let!(:centro_de_custo){ create(:centro_de_custo)}
      before do
        put "/centro_de_custo/#{centro_de_custo.id}", params: {centro: centro_params}.to_json, headers: headers
      end
  
      context 'quando os parametros são inválidos' do
        let(:centro_params){{ descricao: 'Teste de atualização' }}
  
        it 'retornar  status code 200' do
          expect(response).to have_http_status(200)
        end
  
        it 'returnar json de centro de custo' do
          expect(json_body[:descricao]).to eq(centro_params[:descricao])
        end
  
        it 'atualizado centro de custo na base' do
          expect(CentroDeCusto.find_by(descricao: centro_params[:descricao])).not_to be_nil
        end
  
      end
  
      context 'Quando os parametros são inválidos' do
        let(:centro_params){{ descricao: '' }}
  
        it 'retornar status code 422' do
          expect(response).to have_http_status(422)
        end
  
        it 'retornar json de erro no campo descrição' do
             expect(json_body).to have_key(:errors)
        end
  
        it 'não atualizadona base de dados o centro de custo' do
          expect(CentroDeCusto.find_by(descricao: centro_params[:descricao])).to be_nil
        end
      end
    end

      
  describe 'DELETE /CentroDeCusto/:id' do
    let!(:centro_de_custo){ create(:centro_de_custo)}
    before do
      delete "/centro_de_custo/#{centro_de_custo.id}", params: {}, headers: headers
    end

    it 'retornar status code 200' do
      expect(response).to have_http_status(200)
    end

   it 'remover departamento do banco' do
      expect(CentroDeCusto.find_by(id: centro_de_custo.id)).to be_nil
   end

 end
end