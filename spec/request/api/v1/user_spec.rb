require 'rails_helper'

RSpec.describe 'User API', type: :request do
  before { host! 'api.permata.test'}

  let!(:user){ FactoryBot.create(:user)}
  let(:tenant){ FactoryBot.create(:tenant)}
  let(:auth_data){ user.create_new_auth_token}

  let(:headers) do
    {
      'Accept' => 'application/vnd.permata.v1',
      'Content-type' => Mime[:json].to_s,
      'Access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  describe 'GET /auth/validate_token' do
    let(:user){ create(:user)}

    context 'when the request header are valid' do
      before{ get '/auth/validate_token', params:{}, headers: headers }
      it 'return status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'return the user id' do
        expect(json_body[:data][:id]).to eq(user.id)
      end

    end

    context 'when the request header are invalid' do
      before do
        headers['access-token'] = 'invalid_token'
        get '/auth/validate_token', params:{}, headers: headers
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  describe 'POST /auth' do
    before do
      post '/auth', params: user_params.to_json, headers: headers
    end

    context 'when the param are valid' do
      let(:user_params){ FactoryBot.build(:user, tenant_id: tenant.id) }



      it 'return status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'return json for create user' do
        expect(json_body[:data][:email]).to eq(user_params[:email])
      end
    end

    context 'when the params are invalid' do
      let(:user_params){ attributes_for(:user, email: 'invalid_email@')}

      it 'return status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'return the json for the error' do
        expect(json_body).to have_key(:errors)
      end
    end

  end

  describe 'PUT /auth' do
    let!(:user){ create(:user)}
    before do
      put "/auth", params: user_params.to_json, headers: headers
    end

    context 'when the request param are valid' do
      let(:user_params){{ email: 'new_email@permata.com.br'}}
      it 'return status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'return the json data for the update user' do
        expect(json_body[:data][:email]).to eq(user_params[:email])
      end
    end

    context 'when the request params are valid' do
      let(:user_params){{ email: 'invalid_email@'}}

      it 'return status code 422' do
        expect(response).to have_http_status(422)
      end
    end
  end

  describe 'DELETE /auth' do
     before do
       delete '/auth', params: {}, headers: headers
     end

     it 'return status code 200' do
       expect(response).to have_http_status(200)
     end

    it 'removes the user from database' do
       expect(User.find_by(id: user.id)).to be_nil
    end

  end
end
