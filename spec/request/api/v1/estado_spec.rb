require 'rails_helper'

RSpec.describe 'Estado API', type: :request do
    before { host! 'api.permata.test'}

    let(:user){ FactoryBot.create(:user)}
    let(:auth_data){ user.create_new_auth_token }  
  
    let(:headers) do
      {
        'Accept' => 'application/vnd.permata.v1',
        'Content-type' => Mime[:json].to_s,
        'Access-token' => auth_data['access-token'],
        'uid' => auth_data['uid'],
        'client' => auth_data['client']
      }
    end
 
    describe 'GET /Estado' do
         before do
            create_list(:estado, 5)
            get '/estado', params: {}, headers: headers
         end

         it "retornar status code 200" do
              expect(response).to have_http_status(200)
         end         

         it "retornar 5 estados" do
             expect(json_body[:estado].count).to eq(5)
         end         
    end

    describe 'GET /Estado/:id' do
        let(:estado){ create(:estado)}

        before{ get "/estado/#{estado.id}", params:{}, headers: headers}

        it "retornar status code 200" do
            expect(response).to have_http_status(200)
        end

        it "retorna o json do descricao" do
            expect(json_body[:descricao]).to eq(estado.descricao)
        end
        
        it "retornar o json da Sigla" do
            expect(json_body[:sigla]).to eq(estado.sigla)
        end        
    end
    
    

end
