require 'rails_helper'

RSpec.describe 'Departamento API', type: :request do
  before { host! 'api.permata.test'}

  let(:tenant){ FactoryBot.create(:tenant)}
  let(:user){ FactoryBot.create(:user)}
  let(:auth_data){ user.create_new_auth_token }


  let(:headers) do
    {
      'Accept' => 'application/vnd.permata.v1',
      'Content-type' => Mime[:json].to_s,
      'Access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  describe 'GET /Departamentos' do
    before do
      create_list(:departamento, 5)
      get '/departamentos', params: {}, headers: headers
    end

    it 'retornar status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'retornar 5 departamento da base' do
      expect(json_body[:departamento].count).to eq(5)
    end

  end

  describe 'GET /departamentos/:id' do
    let(:departamento){ create(:departamento) }

    before{ get "/departamentos/#{departamento.id}", params: {}, headers: headers }

    it 'retornar status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'retorna o json do departamento' do
      expect(json_body[:descricao]).to eq(departamento.descricao)
    end
  end

  describe 'POST /departamentos' do
    before do
      post '/departamentos', params: {departamento: departamento_params}.to_json, headers: headers
    end

    context 'Quando os parametros forem validos' do
      let(:departamento_params){ FactoryBot.build(:departamento,tenant_id: tenant.id)}

      it 'retornar status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'salvar departamento no banco' do
        expect(Departamento.find_by(descricao: departamento_params[:descricao])).not_to be_nil
      end

      it 'retornar json quando criado o departamento' do
        expect(json_body[:descricao]).to eq(departamento_params[:descricao])
      end
    end

    context 'Quando os parametros não são validos' do
      let(:departamento_params){ attributes_for(:departamento, descricao: '')}

      it 'retornar status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'quando não salvar o departamento no banco' do
        expect(Departamento.find_by(descricao: departamento_params[:descricao])).to be_nil
      end

      it 'retornar json de erro' do
         expect(json_body).to have_key(:errors)
      end
    end
  end

  describe 'PUT /departamento/:id' do
    let!(:departamento){ create(:departamento)}
    before do
      put "/departamentos/#{departamento.id}", params: {departamento: departamento_params}.to_json, headers: headers
    end

    context 'quando os parametros são inválidos' do
      let(:departamento_params){{ descricao: 'Teste de atualização' }}

      it 'retornar  status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returnar json de departamento' do
        expect(json_body[:descricao]).to eq(departamento_params[:descricao])
      end

      it 'atualizado departamento na base' do
        expect(Departamento.find_by(descricao: departamento_params[:descricao])).not_to be_nil
      end

    end

    context 'Quando os parametros são inválidos' do
      let(:departamento_params){{ descricao: '' }}

      it 'retornar status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'retornar json de erro no campo descrição' do
           expect(json_body).to have_key(:errors)
      end

      it 'não atualizadona base de dados o departamento' do
        expect(Departamento.find_by(descricao: departamento_params[:descricao])).to be_nil
      end
    end
  end
  
  describe 'DELETE /departamentos/:id' do
    let!(:departamento){ create(:departamento)}
    before do
      delete "/departamentos/#{departamento.id}", params: {}, headers: headers
    end

    it 'retornar status code 200' do
      expect(response).to have_http_status(200)
    end

   it 'remover departamento do banco' do
      expect(Departamento.find_by(id: departamento.id)).to be_nil
   end

 end


end
