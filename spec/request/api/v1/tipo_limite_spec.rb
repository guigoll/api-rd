require 'rails_helper'

RSpec.describe "Tipo Limite API", type: :request do
    before { host! 'api.permata.test'}

    let(:user){ FactoryBot.create(:user)}
    let(:auth_data){ user.create_new_auth_token }  
  
    let(:headers) do
      {
        'Accept' => 'application/vnd.permata.v1',
        'Content-type' => Mime[:json].to_s,
        'Access-token' => auth_data['access-token'],
        'uid' => auth_data['uid'],
        'client' => auth_data['client']
      }
    end

    describe 'GET /tipo_limite' do
        before do
           create_list(:tipo_limite, 5)
           get '/tipo_limite', params: {}, headers: headers
        end

        it "retornar status code 200" do
             expect(response).to have_http_status(200)
        end         

        it "retornar 5 tipo de limite" do
            expect(json_body[:tipo_limite].count).to eq(5)
        end         
   end

   describe 'GET /tipo_limite/:id' do
     let(:tipo_limite){ create(:tipo_limite)}

     before{ get "/tipo_limite/#{tipo_limite.id}", params:{}, headers: headers}

     it "retornar status code 200" do
         expect(response).to have_http_status(200)
     end

     it "retorna o json do descricao" do
         expect(json_body[:descricao]).to eq(tipo_limite.descricao)
     end
  end   

end
