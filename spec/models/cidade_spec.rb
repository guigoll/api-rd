require 'rails_helper'

RSpec.describe Cidade, type: :model do
  let(:cidade){ FactoryBot.build(:cidade)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:estado)}
  it{ is_expected.to have_many(:enderecos)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
  it{ is_expected.to respond_to(:estado_id)}
end
