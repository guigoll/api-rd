require 'rails_helper'

RSpec.describe Tenant, type: :model do
  let(:tenant){ FactoryBot.build(:tenant)}

  #verifica os relacionamentos
  it{ is_expected.to have_many(:pessoas)}
  it{ is_expected.to have_many(:departamentos)}
  it{ is_expected.to have_many(:centro_de_custos)}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:codigocliente)}
end
