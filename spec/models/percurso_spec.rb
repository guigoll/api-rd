require 'rails_helper'

RSpec.describe Percurso, type: :model do
  let(:percurso){ FactoryBot.build(:percurso)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:centro_de_custo)}
  it{ is_expected.to belong_to(:tipo_pagamento)}
  it{ is_expected.to belong_to(:trecho_pre_cadastrado)}

  it{ is_expected.to have_one(:relatorio_despesa)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
  it{ is_expected.to validate_presence_of :reembolsavel}
  it{ is_expected.to validate_presence_of :centro_de_custo_id}
  it{ is_expected.to validate_presence_of :tipo_pagamento_id}
  it{ is_expected.to validate_presence_of :trecho_pre_cadastrado_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
  it{ is_expected.to respond_to(:reembolsavel)}
  it{ is_expected.to respond_to(:observacao)}
  it{ is_expected.to respond_to(:centro_de_custo_id)}
  it{ is_expected.to respond_to(:tipo_pagamento_id)}
  it{ is_expected.to respond_to(:trecho_pre_cadastrado_id)}
end
