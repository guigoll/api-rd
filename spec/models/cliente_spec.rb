require 'rails_helper'

RSpec.describe Cliente, type: :model do
  let(:cliente){ FactoryBot.build(:cliente)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:pessoa)}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:pessoa_id)}
end
