require 'rails_helper'

RSpec.describe Despesa, type: :model do
  let(:despesa){ FactoryBot.build(:despesa)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:moeda)}
  it{ is_expected.to belong_to(:centro_de_custo)}
  it{ is_expected.to belong_to(:tipo_pagamento)}
  it{ is_expected.to belong_to(:tipo_de_despesa)}
  it{ is_expected.to belong_to(:politica_de_despesa)}
  it{ is_expected.to belong_to(:funcionario)}

  it{ is_expected.to have_one(:relatorio_despesa)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
  it{ is_expected.to validate_presence_of :reembolsavel}  
  it{ is_expected.to validate_presence_of :moeda_id}
  it{ is_expected.to validate_presence_of :centro_de_custo_id}
  it{ is_expected.to validate_presence_of :tipo_pagamento_id}
  it{ is_expected.to validate_presence_of :tipo_de_despesa_id}
  it{ is_expected.to validate_presence_of :politica_de_despesa_id}
  it{ is_expected.to validate_presence_of :funcionario_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
  it{ is_expected.to respond_to(:reembolsavel)}
  it{ is_expected.to respond_to(:observacao)}
  it{ is_expected.to respond_to(:moeda_id)}
  it{ is_expected.to respond_to(:centro_de_custo_id)}
  it{ is_expected.to respond_to(:tipo_pagamento_id)}
  it{ is_expected.to respond_to(:tipo_de_despesa_id)}
  it{ is_expected.to respond_to(:politica_de_despesa_id)}
  it{ is_expected.to respond_to(:funcionario_id)}
end
