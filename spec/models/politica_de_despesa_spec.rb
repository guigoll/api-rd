require 'rails_helper'

RSpec.describe PoliticaDeDespesa, type: :model do
  let(:politica_de_despesa){ FactoryBot.build(:politica_de_despesa)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:tenant)}

  it{ is_expected.to have_many(:despesas)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
  it{ is_expected.to validate_presence_of :limitemaximodediaspassados}
  it{ is_expected.to validate_presence_of :tenant_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
  it{ is_expected.to respond_to(:limitemaximodediaspassados)} 
  it{ is_expected.to respond_to(:tenant_id)} 
 
end
