require 'rails_helper'

RSpec.describe Adiantamento, type: :model do
  let(:adiantamento){ FactoryBot.build(:adiantamento)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:moeda)}
  it{ is_expected.to belong_to(:tipo_de_adiantamento)}

  it{ is_expected.to have_one(:relatorio_despesa)}

   it{ is_expected.to have_many(:adiantamento_funcionarios)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :valor}
  it{ is_expected.to validate_presence_of :finalidade}
  it{ is_expected.to validate_presence_of :datanecessidade}
  it{ is_expected.to validate_presence_of :funcionario_id}
  it{ is_expected.to validate_presence_of :moeda_id}
  it{ is_expected.to validate_presence_of :tipo_de_adiantamento_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:datanecessidade)}
  it{ is_expected.to respond_to(:finalidade)}
  it{ is_expected.to respond_to(:valor)}
  it{ is_expected.to respond_to(:observacao)}
  it{ is_expected.to respond_to(:moeda_id)}
  it{ is_expected.to respond_to(:tipo_de_adiantamento_id)}
end
