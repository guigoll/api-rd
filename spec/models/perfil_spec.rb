require 'rails_helper'

RSpec.describe Perfil, type: :model do
  let(:perfil){ FactoryBot.build(:perfil)}

  #verifica os relacionamentos
  it{ is_expected.to have_many(:funcionario_empresas )}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
 
  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
end
