require 'rails_helper'

RSpec.describe Quilometragem, type: :model do
  let(:quilometragem){ FactoryBot.build(:quilometragem)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:politica_de_quilometragem)}
  it{ is_expected.to belong_to(:tenant)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
  it{ is_expected.to validate_presence_of :valorpagoporkm}
  it{ is_expected.to validate_presence_of :permitiralterarvalormanualmente}
  it{ is_expected.to validate_presence_of :exigirimagem}
  it{ is_expected.to validate_presence_of :tenant_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
  it{ is_expected.to respond_to(:valorpagoporkm)}
  it{ is_expected.to respond_to(:permitiralterarvalormanualmente)}
  it{ is_expected.to respond_to(:exigirimagem)}
  it{ is_expected.to respond_to(:politica_de_quilometragem_id)}
  it{ is_expected.to respond_to(:tenant_id)} 
end
