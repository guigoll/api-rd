require 'rails_helper'

RSpec.describe CartaoOperadora, type: :model do
  let(:cartao_operadora){ FactoryBot.build(:cartao_operadora)}

    #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
end
