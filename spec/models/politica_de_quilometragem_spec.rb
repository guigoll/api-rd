require 'rails_helper'

RSpec.describe PoliticaDeQuilometragem, type: :model do
  let(:politica_de_quilometragem){ FactoryBot.build(:politica_de_quilometragem)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:tenant)}

  it{ is_expected.to have_many(:quilometragems)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :valorpagoporkm}
  it{ is_expected.to validate_presence_of :permitiralterarvalormanualmente}
  it{ is_expected.to validate_presence_of :tenant_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:valorpagoporkm)}
  it{ is_expected.to respond_to(:permitiralterarvalormanualmente)} 
  it{ is_expected.to respond_to(:tenant_id)}
 
end
