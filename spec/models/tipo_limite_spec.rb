require 'rails_helper'

RSpec.describe TipoLimite, type: :model do
  let(:tipo_limite){ FactoryBot.build(:tipo_limite)}

  #verifica os relacionamentos
  it{ is_expected.to have_many(:tipo_de_despesas )}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
 
  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}

end
