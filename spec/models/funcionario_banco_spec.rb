require 'rails_helper'

RSpec.describe FuncionarioBanco, type: :model do
  let(:funcionario_banco){ FactoryBot.build(:funcionario_banco)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:banco)}
  it{ is_expected.to belong_to(:funcionario)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :banco_id}
  it{ is_expected.to validate_presence_of :funcionario_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:banco_id)}
  it{ is_expected.to respond_to(:funcionario_id)}
 
end
