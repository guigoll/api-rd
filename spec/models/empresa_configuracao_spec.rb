require 'rails_helper'

RSpec.describe EmpresaConfiguracao, type: :model do
  let(:empresa_configuracao){ FactoryBot.build(:empresa_configuracao)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:empresa)}
  
  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :permitiralteracaomembrosequipeapenasporadmin}
  it{ is_expected.to validate_presence_of :permitirenviodeemaildeaprovacaoderelatorio}
  it{ is_expected.to validate_presence_of :descricaodespesaobrigatoria}
  it{ is_expected.to validate_presence_of :tornacampoformadepagtoobrigatorio}
  it{ is_expected.to validate_presence_of :permitirlancamentoscomvalornegativo}
  it{ is_expected.to validate_presence_of :tornarcampoprojetodadespesaobrigatorio}
  it{ is_expected.to validate_presence_of :permitirutiliaradiantamento}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:permitiralteracaomembrosequipeapenasporadmin)}
  it{ is_expected.to respond_to(:permitirenviodeemaildeaprovacaoderelatorio)}
  it{ is_expected.to respond_to(:descricaodespesaobrigatoria)}
  it{ is_expected.to respond_to(:tornacampoformadepagtoobrigatorio)}
  it{ is_expected.to respond_to(:permitirlancamentoscomvalornegativo)}
  it{ is_expected.to respond_to(:tornarcampoprojetodadespesaobrigatorio)}
  it{ is_expected.to respond_to(:permitirutiliaradiantamento)}
  it{ is_expected.to respond_to(:empresa_id)}
 
end
