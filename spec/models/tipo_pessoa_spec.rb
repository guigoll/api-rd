require 'rails_helper'

RSpec.describe TipoPessoa, type: :model do
  let(:tipo_pessoa){ FactoryBot.build(:tipo_pessoa)}

  #verifica os relacionamentos
  it{ is_expected.to have_many(:pessoas)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
 
  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
end
