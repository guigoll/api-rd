require 'rails_helper'

RSpec.describe Aprovador, type: :model do
  let(:aprovador){ FactoryBot.build(:aprovador)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:funcionario)}
  it{ is_expected.to belong_to(:fluxo_de_aprovacao)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :condicao}
  it{ is_expected.to validate_presence_of :nivel}
  it{ is_expected.to validate_presence_of :grupo}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:condicao)}
  it{ is_expected.to respond_to(:nivel)}
  it{ is_expected.to respond_to(:grupo)}
  it{ is_expected.to respond_to(:funcionario_id)}
  it{ is_expected.to respond_to(:fluxo_de_aprovacao_id)}

end
