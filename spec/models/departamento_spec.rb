require 'rails_helper'

RSpec.describe Departamento, type: :model do
  let(:departamento){ FactoryBot.build(:departamento)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:tenant)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
  it{ is_expected.to validate_presence_of :tenant_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
  it{ is_expected.to respond_to(:integracao)}
  it{ is_expected.to respond_to(:tenant_id)}
 
end
