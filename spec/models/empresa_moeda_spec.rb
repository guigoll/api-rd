require 'rails_helper'

RSpec.describe EmpresaMoeda, type: :model do
  let(:empresa_moeda){ FactoryBot.build(:empresa_moeda)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:moeda)}
  it{ is_expected.to belong_to(:empresa)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :moeda_id}
  it{ is_expected.to validate_presence_of :empresa_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:moeda_id)}
  it{ is_expected.to respond_to(:empresa_id)}

end
