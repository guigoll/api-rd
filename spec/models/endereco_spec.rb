require 'rails_helper'

RSpec.describe Endereco, type: :model do
  let(:endereco){ FactoryBot.build(:endereco)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:cidade)}
  it{ is_expected.to belong_to(:pessoa)}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:logadouro)}
  it{ is_expected.to respond_to(:numero)}
  it{ is_expected.to respond_to(:complemento)}
  it{ is_expected.to respond_to(:bairo)}
  it{ is_expected.to respond_to(:cep)}
  it{ is_expected.to respond_to(:cidade_id)}
  it{ is_expected.to respond_to(:pessoa_id)}
 
end
