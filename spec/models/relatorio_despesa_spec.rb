require 'rails_helper'

RSpec.describe RelatorioDespesa, type: :model do
  let(:relatorio_despesa){ FactoryBot.build(:relatorio_despesa)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:despesa)}
  it{ is_expected.to belong_to(:adiantamento)}
  it{ is_expected.to belong_to(:relatorio_de_despesa)}
  it{ is_expected.to belong_to(:percurso)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :relatorio_de_despesa_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:despesa_id)} 
  it{ is_expected.to respond_to(:adiantamento_id)}
  it{ is_expected.to respond_to(:relatorio_de_despesa_id)} 
  it{ is_expected.to respond_to(:percurso)} 
  
end
