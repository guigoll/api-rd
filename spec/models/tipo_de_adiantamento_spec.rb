require 'rails_helper'

RSpec.describe TipoDeAdiantamento, type: :model do
  let(:tipo_de_adiantamento){ FactoryBot.build(:tipo_de_adiantamento)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:tenant)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
  it{ is_expected.to validate_presence_of :valorlimite}
  it{ is_expected.to validate_presence_of :fundofixo}
  it{ is_expected.to validate_presence_of :tenant_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
  it{ is_expected.to respond_to(:valorlimite)} 
  it{ is_expected.to respond_to(:fundofixo)} 
  it{ is_expected.to respond_to(:tenant_id)} 
end
