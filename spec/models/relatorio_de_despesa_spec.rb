require 'rails_helper'

RSpec.describe RelatorioDeDespesa, type: :model do
  let(:relatorio_de_despesa){ FactoryBot.build(:relatorio_de_despesa)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:fluxo_de_aprovacao)}
  it{ is_expected.to belong_to(:funcionario)}

  it{ is_expected.to have_one(:relatorio_despesa)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
  it{ is_expected.to validate_presence_of :funcionario_id}
  it{ is_expected.to validate_presence_of :fluxo_de_aprovacao_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
  it{ is_expected.to respond_to(:envioparaaprovacao)} 
  it{ is_expected.to respond_to(:funcionario_id)} 
  it{ is_expected.to respond_to(:fluxo_de_aprovacao_id)} 
 
end
