require 'rails_helper'

RSpec.describe FluxoDeAprovacao, type: :model do
  let(:fluxo_de_aprovacao){ FactoryBot.build(:fluxo_de_aprovacao)}

  #verifica os relacionamentos
  it{ is_expected.to have_many(:relatorio_de_despesas)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
 
end
