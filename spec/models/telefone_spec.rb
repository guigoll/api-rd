require 'rails_helper'

RSpec.describe Telefone, type: :model do
  let(:telefone){ FactoryBot.build(:telefone)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:pessoa)}  

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :pessoa_id}
  it{ is_expected.to validate_presence_of :numero}
 
  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:numero)}
  it{ is_expected.to respond_to(:pessoa_id)} 
 
end
