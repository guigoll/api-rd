require 'rails_helper'

RSpec.describe ProjetoFuncionario, type: :model do
  let(:projeto_funcionario){ FactoryBot.build(:projeto_funcionario)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:projeto)}
  it{ is_expected.to belong_to(:funcionario)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :projeto_id}
  it{ is_expected.to validate_presence_of :funcionario_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:projeto_id)}
  it{ is_expected.to respond_to(:funcionario_id)} 
end
