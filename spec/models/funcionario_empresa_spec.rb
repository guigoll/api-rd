require 'rails_helper'

RSpec.describe FuncionarioEmpresa, type: :model do
  let(:funcionario_empresa){ FactoryBot.build(:funcionario_empresa)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:funcionario)}
  it{ is_expected.to belong_to(:empresa)}
  it{ is_expected.to belong_to(:perfil)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :funcionario_id}
  it{ is_expected.to validate_presence_of :empresa_id}
  it{ is_expected.to validate_presence_of :perfil_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:funcionario_id)}
  it{ is_expected.to respond_to(:empresa_id)}
  it{ is_expected.to respond_to(:perfil_id)}
end
