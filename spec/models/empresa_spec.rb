require 'rails_helper'

RSpec.describe Empresa, type: :model do
  let(:empresa){ FactoryBot.build(:empresa)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:pessoa)}
  it{ is_expected.to belong_to(:tenant)}
  it{ is_expected.to have_one(:empresa_configuracao)}
  it{ is_expected.to have_many(:empresa_moedas)}
  it{ is_expected.to have_many(:funcionario_empresas)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :matriz}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:pessoa_id)}
  it{ is_expected.to respond_to(:tenant_id)}
end
