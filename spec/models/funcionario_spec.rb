require 'rails_helper'

RSpec.describe Funcionario, type: :model do
  let(:funcionario){ FactoryBot.build(:funcionario)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:pessoa)}
  it{ is_expected.to belong_to(:centro_de_custo)}
  it{ is_expected.to belong_to(:departamento)}
  it{ is_expected.to belong_to(:politica_de_despesa)}
  it{ is_expected.to belong_to(:fluxo_de_aprovacao)}
  it{ is_expected.to belong_to(:politica_de_quilometragem)}

  it{ is_expected.to have_one(:aprovador)}
  it{ is_expected.to have_one(:funcionario_cartao)}
  
  it{ is_expected.to have_many(:despesas)}
  it{ is_expected.to have_many(:adiantamento_funcionarios)}
  it{ is_expected.to have_many(:funcionario_bancos)}
  it{ is_expected.to have_many(:funcionario_empresas)}
  it{ is_expected.to have_many(:projeto_funcionarios)}  

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :pessoa_id}
  it{ is_expected.to validate_presence_of :centro_de_custo_id}
  it{ is_expected.to validate_presence_of :departamento_id}
  it{ is_expected.to validate_presence_of :politica_de_despesa_id}
  it{ is_expected.to validate_presence_of :fluxo_de_aprovacao_id}
  it{ is_expected.to validate_presence_of :politica_de_quilometragem_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:pessoa_id)}
  it{ is_expected.to respond_to(:centro_de_custo_id)}
  it{ is_expected.to respond_to(:departamento_id)}
  it{ is_expected.to respond_to(:integracao)}
  it{ is_expected.to respond_to(:politica_de_despesa_id)}
  it{ is_expected.to respond_to(:fluxo_de_aprovacao_id)}
  it{ is_expected.to respond_to(:politica_de_quilometragem_id)}
end
