require 'rails_helper'

RSpec.describe Moeda, type: :model do
  let(:moeda){ FactoryBot.build(:moeda)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:tenant)}
  
  it{ is_expected.to have_many(:despesas)}
  it{ is_expected.to have_many(:empresa_moedas)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
  it{ is_expected.to validate_presence_of :tenant_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
  it{ is_expected.to respond_to(:tenant_id)}
end
