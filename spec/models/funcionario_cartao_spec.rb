require 'rails_helper'

RSpec.describe FuncionarioCartao, type: :model do
  let(:funcionario_cartao){ FactoryBot.build(:funcionario_cartao)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:cartao_operadora)}
  it{ is_expected.to belong_to(:funcionario)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :numero}
  it{ is_expected.to validate_presence_of :digito}
  it{ is_expected.to validate_presence_of :ativo}
  it{ is_expected.to validate_presence_of :limite}
  it{ is_expected.to validate_presence_of :funcionario_id}
  it{ is_expected.to validate_presence_of :cartao_operadora_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:numero)}
  it{ is_expected.to respond_to(:digito)}
  it{ is_expected.to respond_to(:ativo)}
  it{ is_expected.to respond_to(:limite)}
  it{ is_expected.to respond_to(:funcionario_id)}
  it{ is_expected.to respond_to(:cartao_operadora_id)}
end
