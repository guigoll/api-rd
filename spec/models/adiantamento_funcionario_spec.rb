require 'rails_helper'

RSpec.describe AdiantamentoFuncionario, type: :model do
  let(:adiantamento_funcionario){ FactoryBot.build(:adiantamento_funcionario)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:adiantamento)}
  it{ is_expected.to belong_to(:funcionario)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :adiantamento_id}
  it{ is_expected.to validate_presence_of :funcionario_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:adiantamento_id)}
  it{ is_expected.to respond_to(:funcionario_id)} 
 
end




