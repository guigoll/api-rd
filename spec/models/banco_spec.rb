require 'rails_helper'

RSpec.describe Banco, type: :model do
  let(:banco){ FactoryBot.build(:banco)}

  #verifica os relacionamentos
  it{ is_expected.to have_many(:funcionario_bancos)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
  it{ is_expected.to validate_presence_of :codigobc}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
  it{ is_expected.to respond_to(:codigobc)}

end
