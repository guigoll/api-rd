require 'rails_helper'

RSpec.describe Pai, type: :model do
  #pai = pais
  let(:pai){ FactoryBot.build(:pai)}

  #verifica os relacionamentos
  it{ is_expected.to have_many(:estados)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
end
