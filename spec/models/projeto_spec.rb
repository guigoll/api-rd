require 'rails_helper'

RSpec.describe Projeto, type: :model do
  let(:projeto){ FactoryBot.build(:projeto)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:cliente)}
  it{ is_expected.to belong_to(:tenant)}

  it{ is_expected.to have_many(:projeto_funcionarios)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
  it{ is_expected.to validate_presence_of :cliente_id}
  it{ is_expected.to validate_presence_of :tenant_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
  it{ is_expected.to respond_to(:cliente_id)}
  it{ is_expected.to respond_to(:tenant_id)} 
end
