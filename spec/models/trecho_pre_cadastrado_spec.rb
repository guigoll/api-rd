require 'rails_helper'

RSpec.describe TrechoPreCadastrado, type: :model do
  let(:trecho_pre_cadastrado){ FactoryBot.build(:trecho_pre_cadastrado)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:politica_de_quilometragem)}
  it{ is_expected.to belong_to(:tenant )}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :cidadeorigem}
  it{ is_expected.to validate_presence_of :cidadedestino}
  it{ is_expected.to validate_presence_of :politica_de_quilometragem_id}
  it{ is_expected.to validate_presence_of :tenant_id}
 
  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:cidadeorigem)}
  it{ is_expected.to respond_to(:cidadedestino)}
  it{ is_expected.to respond_to(:politica_de_quilometragem_id)}
  it{ is_expected.to respond_to(:tenant_id)}
end
