require 'rails_helper'

RSpec.describe Estado, type: :model do
  let(:estado){ FactoryBot.build(:estado)}

  #verifica os relacionamentos
  # it{ is_expected.to belong_to(:pai)}
  it{ is_expected.to have_many(:cidades)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
  it{ is_expected.to validate_presence_of :sigla}
  it{ is_expected.to validate_presence_of :pai_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
  it{ is_expected.to respond_to(:sigla)}
  it{ is_expected.to respond_to(:pai_id)}
 
end
