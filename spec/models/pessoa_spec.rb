require 'rails_helper'

RSpec.describe Pessoa, type: :model do
  let(:pessoa){ FactoryBot.build(:pessoa)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:tipo_pessoa)}
  it{ is_expected.to belong_to(:tenant)}

  it{ is_expected.to have_one(:cliente)}
  it{ is_expected.to have_one(:funcionario)}

  it{ is_expected.to have_many(:telefones)}
  it{ is_expected.to have_many(:enderecos)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :tipo_pessoa_id}
  it{ is_expected.to validate_presence_of :ativo}
  it{ is_expected.to validate_presence_of :tenant_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:tipo_pessoa_id)}
  it{ is_expected.to respond_to(:ativo)}
  it{ is_expected.to respond_to(:email)}
  it{ is_expected.to respond_to(:tenant_id)}
end
