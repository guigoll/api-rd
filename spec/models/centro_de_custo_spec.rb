require 'rails_helper'

RSpec.describe CentroDeCusto, type: :model do
  let(:centro_de_custo){ FactoryBot.build(:centro_de_custo)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:tenant)}
  it{ is_expected.to have_many(:despesas) } 

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
  it{ is_expected.to respond_to(:integracao)}
  it{ is_expected.to respond_to(:tenant_id)}
end
