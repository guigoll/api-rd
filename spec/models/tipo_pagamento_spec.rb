require 'rails_helper'

RSpec.describe TipoPagamento, type: :model do
  let(:tipo_pagamento){ FactoryBot.build(:tipo_pagamento)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:moeda)}
  it{ is_expected.to belong_to(:tenant)}

  it{ is_expected.to have_many(:despesas)}

  #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
  it{ is_expected.to validate_presence_of :reembolsavel}
  it{ is_expected.to validate_presence_of :baixardoadiantamento}
  it{ is_expected.to validate_presence_of :moeda_id}
  it{ is_expected.to validate_presence_of :tenant_id}
 
  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
  it{ is_expected.to respond_to(:reembolsavel)}
  it{ is_expected.to respond_to(:baixardoadiantamento)}
  it{ is_expected.to respond_to(:moeda_id)}
  it{ is_expected.to respond_to(:tenant_id)}
end
