require 'rails_helper'

RSpec.describe TipoDeDespesa, type: :model do
  let(:tipo_de_despesa){ FactoryBot.build(:tipo_de_despesa)}

  #verifica os relacionamentos
  it{ is_expected.to belong_to(:tipo_limite)}
  it{ is_expected.to belong_to(:tipo_limite_de_lancamento)}
  it{ is_expected.to belong_to(:tenant)}

  it{ is_expected.to have_many(:despesas)}

    #verifica os campos orbigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :descricao}
  it{ is_expected.to validate_presence_of :tipo_limite_id}
  it{ is_expected.to validate_presence_of :tipo_limite_de_lancamento_id}
  it{ is_expected.to validate_presence_of :permitirultrapassarlimite}
  it{ is_expected.to validate_presence_of :exigerecibo}
  it{ is_expected.to validate_presence_of :observacaoobrigatorio}
  it{ is_expected.to validate_presence_of :tenant_id}

  #verifica se os campo ainda existe no banco
  it{ is_expected.to respond_to(:descricao)}
  it{ is_expected.to respond_to(:tipo_limite_id)} 
  it{ is_expected.to respond_to(:tipo_limite_de_lancamento_id)}
  it{ is_expected.to respond_to(:permitirultrapassarlimite)} 
  it{ is_expected.to respond_to(:exigerecibo)}
  it{ is_expected.to respond_to(:tenant_id)} 
end
