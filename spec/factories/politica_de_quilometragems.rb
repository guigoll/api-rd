FactoryBot.define do
  factory :politica_de_quilometragem do
    valorpagoporkm { Faker::Commerce.price(range = 0..10.0, as_string = true) }
    permitiralterarvalormanualmente { Faker::Boolean.boolean }
    tenant
  end
end
