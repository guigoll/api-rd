FactoryBot.define do
  factory :funcionario do
    pessoa 
    centrodecusto 
    departamento 
    integracao { Faker::Code.asin }
    politicadedespesa 
    fluxodeaprovacao 
    politicadequilometragem
  end
end
