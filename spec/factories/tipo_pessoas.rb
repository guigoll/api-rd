FactoryBot.define do
  factory :tipo_pessoa do
    descricao { Faker::Lorem.word }
  end
end
