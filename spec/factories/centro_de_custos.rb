FactoryBot.define do
  factory :centro_de_custo do
    descricao { Faker::Commerce.department }
    integracao { Faker::Code.asin }
    tenant
  end
end
