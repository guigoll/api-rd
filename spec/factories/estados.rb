FactoryBot.define do
  factory :estado do
    descricao { Faker::Address.state }
    sigla { Faker::Address.state_abbr }
    pai
  end
end
