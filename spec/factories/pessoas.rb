FactoryBot.define do
  factory :pessoa do
    tipopessoa 
    ativo { Faker::Boolean.boolean }
  end
end
