FactoryBot.define do
  factory :relatorio_de_despesa do
    descricao { "MyString" }
    envioparaaprovacao { Faker::Date.forward(23)  }
    funcionario
    fluxo_de_paprovacao    
  end
end
