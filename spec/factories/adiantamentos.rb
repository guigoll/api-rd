FactoryBot.define do
  factory :adiantamento do
    funcionario
    datanecessidade { "2019-05-05 13:32:20" }
    finalidade { Faker::Lorem.word}
    valor { Faker::Commerce.price }
    observacao { Faker::Lorem.paragraph_by_chars(256, false)  }
    moeda
    tipoadiantamento 
  end
end
