FactoryBot.define do
  factory :despesa do
    descricao { Faker::Lorem.sentence }
    moeda
    centrodecusto
    tipodepagamento
    tipodespesa 
    observacao { Faker::Lorem.paragraph_by_chars(256, false) }
    reembolsavel { Faker::Boolean.boolean }
    politicadedespesa
    funcionario
  end
end
