FactoryBot.define do
  factory :tipo_limite_de_lancamento do
    descricao { Faker::Lorem.word }
  end
end
