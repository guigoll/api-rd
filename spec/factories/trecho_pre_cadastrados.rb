FactoryBot.define do
  factory :trecho_pre_cadastrado do
    cidadeorigen { 1 }
    cidadedestino { 2 }
    politicadequilometragem
    tenant
  end
end
