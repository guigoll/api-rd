FactoryBot.define do
  factory :adiantamento_funcionario do
    funcionario 
    adiantamento 
    utilizado { Faker::Boolean.boolean }
  end
end
