FactoryBot.define do
  factory :aprovador do
    condicao { "MyString" }
    nivel { Faker::Number.decimal_part(2) }
    grupo { Faker::Number.hexadecimal(3) }
    funcionario 
    fluxodeaprovacao 
  end
end
