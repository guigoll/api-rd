FactoryBot.define do
  factory :tipo_limite do
    descricao { Faker::Lorem.word }
  end
end
