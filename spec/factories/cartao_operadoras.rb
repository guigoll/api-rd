FactoryBot.define do
  factory :cartao_operadora do
    descricao { Faker::Business.credit_card_type }
  end
end
