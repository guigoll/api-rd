FactoryBot.define do
  factory :empresa do
    pessoa 
    matriz { Faker::Boolean.boolean }
    tenant
  end
end
