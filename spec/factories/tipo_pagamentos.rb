FactoryBot.define do
  factory :tipo_pagamento do
    descricao { Faker::Lorem.sentence }
    reembolsavel { Faker::Boolean.boolean  }
    baixardoadiantamento { Faker::Boolean.boolean  }
    moeda
    tenant 
  end
end
