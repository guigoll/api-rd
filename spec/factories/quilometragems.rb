FactoryBot.define do
  factory :quilometragem do
    descricao { Faker::Lorem.sentence }
    valorpagoporkm { Faker::Commerce.price(range = 0..10.0, as_string = true) }
    permitiralterarvalormanualmente { Faker::Boolean.boolean }
    exigirimagem { Faker::Boolean.boolean }
    politica_de_quilometragem 
    tenant
  end
end
