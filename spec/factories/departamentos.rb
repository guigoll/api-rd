FactoryBot.define do
  factory :departamento do
    descricao { Faker::Commerce.department  }
    integracao { Faker::Code.asin }
    tenant
  end
end
