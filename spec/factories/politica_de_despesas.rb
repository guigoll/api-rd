FactoryBot.define do
  factory :politica_de_despesa do
    descricao { Faker::Lorem.sentence }
    limitemaximodediaspassados { Faker::Number.decimal_part(2) }
    tenant
  end
end
