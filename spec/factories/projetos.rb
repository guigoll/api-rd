FactoryBot.define do
  factory :projeto do
    descricao { Faker::Lorem.sentence }
    cliente
    tenant
  end
end
