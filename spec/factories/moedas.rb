FactoryBot.define do
  factory :moeda do
    descricao { Faker::Currency.code }
    tenant
  end
end
