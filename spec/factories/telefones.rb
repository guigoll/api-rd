FactoryBot.define do
  factory :telefone do
    numero { Faker::PhoneNumber.cell_phone  }
    pessoa
  end
end
