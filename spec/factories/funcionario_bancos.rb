FactoryBot.define do
  factory :funcionario_banco do
    agencia { Faker::Bank.routing_number }
    conta { Faker::Bank.account_number }
    digito { Faker::Number.digit }
    funcionario
    banco
  end
end
