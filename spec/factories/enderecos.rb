FactoryBot.define do
  factory :endereco do
    logadouro { Faker::Address.street_name }
    numero { Faker::Address.building_number }
    complemento { Faker::Address.secondary_address }
    bairo { Faker::Address.community  }
    cep { Faker::Address.postcode}
    cidade 
    pessoa
  end
end
