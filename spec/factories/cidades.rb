FactoryBot.define do
  factory :cidade do
    descricao { Faker::Address.city }
    estado
  end
end
