FactoryBot.define do
  factory :funcionario_cartao do
    cartaooperadora
    numero { Faker::Bank.account_number(13) }
    digito { Faker::Number.digit }
    ativo { Faker::Boolean.boolean }
    limite { Faker::Commerce.price(range = 800..10000.0, as_string = true) }
    funcionario 
  end
end
