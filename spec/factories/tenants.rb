FactoryBot.define do
  factory :tenant do
    codigocliente { Faker::Number.leading_zero_number(10) }
  end
end
