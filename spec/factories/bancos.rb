FactoryBot.define do
  factory :banco do
    descricao { Faker::Bank.name }
    codigobc { Faker::Number.decimal_part(2) }
  end
end
