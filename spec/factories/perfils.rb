FactoryBot.define do
  factory :perfil do
    sequence(:descricao, 'A') {|n| "Admin #{n}" }
  end
end
