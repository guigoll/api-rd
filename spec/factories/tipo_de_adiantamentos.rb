FactoryBot.define do
  factory :tipo_de_adiantamento do
    descricao { Faker::Lorem.sentence }
    valorlimite { Faker::Commerce.price(range = 0..10.0, as_string = true) }
    fundofixo { Faker::Boolean.boolean }
    tenant
  end
end
