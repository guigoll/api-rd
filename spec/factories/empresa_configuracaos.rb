FactoryBot.define do
  factory :empresa_configuracao do
    empresa 
    permitiralteracaomembrosequipeapenasporadmin { Faker::Boolean.boolean }
    permitirenviodeemaildeaprovacaoderelatorio { Faker::Boolean.boolean }
    descricaodespesaobrigatoria { Faker::Boolean.boolean }
    tornacampoformadepagtoobrigatorio { Faker::Boolean.boolean }
    permitirlancamentoscomvalornegativo { Faker::Boolean.boolean }
    tornarcampoprojetodadespesaobrigatorio { Faker::Boolean.boolean }
    permitirutiliaradiantamento { Faker::Boolean.boolean }
  end
end
