FactoryBot.define do
  factory :tipo_de_despesa do
    descricao { Faker::Lorem.sentence }
    tipo_limite 
    tipo_limite_de_lancamento
    permitirultrapassarlimite { Faker::Boolean.boolean }
    observacaoobrigatorio { Faker::Boolean.boolean }
    exigerecibo { Faker::Boolean.boolean }
    tenant 
  end
end
