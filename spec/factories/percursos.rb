FactoryBot.define do
  factory :percurso do
    descricao { Faker::Lorem.sentence }
    reembolsavel { Faker::Boolean.boolean }
    observacao { Faker::Lorem.paragraph_by_chars }
    relatorio_de_despesas 
    centro_de_custo 
    tipo_pagamento 
    trecho_pre_cadastrado 
  end
end
