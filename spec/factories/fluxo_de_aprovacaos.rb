FactoryBot.define do
  factory :fluxo_de_aprovacao do
    descricao { Faker::Lorem.sentences }
  end
end
