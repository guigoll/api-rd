class CreateQuilometragems < ActiveRecord::Migration[5.2]
  def change
    create_table  :quilometragems do |t|
      t.string :descricao
      t.decimal :valorpagoporkm
      t.boolean :permitiralterarvalormanualmente
      t.boolean :exigirimagem
      t.references :politica_de_quilometragem, foreign_key: false
      t.references :tenant, foreign_key: true

      t.timestamps
    end
  end
end
