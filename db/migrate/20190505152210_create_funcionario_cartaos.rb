class CreateFuncionarioCartaos < ActiveRecord::Migration[5.2]
  def change
    create_table :funcionario_cartaos do |t|
      t.references :cartao_operadora, foreign_key: true
      t.integer :numero
      t.integer :digito
      t.boolean :ativo
      t.decimal :limite
      t.references :funcionario, foreign_key: true

      t.timestamps 
    end
  end
end
