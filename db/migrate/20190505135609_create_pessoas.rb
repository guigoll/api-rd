class CreatePessoas < ActiveRecord::Migration[5.2]
  def change
    create_table :pessoas do |t|
      t.references :tipo_pessoa, foreign_key: true
      t.references :tenant, foreign_key: true
      t.boolean :ativo
      t.string :email

      t.timestamps
    end
  end
end
