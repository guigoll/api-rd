class CreateFluxoDeAprovacaos < ActiveRecord::Migration[5.2]
  def change
    create_table  :fluxo_de_aprovacaos do |t|
      t.string :descricao

      t.timestamps
    end
  end
end
