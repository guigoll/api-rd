class CreateTrechoPreCadastrados < ActiveRecord::Migration[5.2]
  def change
    create_table :trecho_pre_cadastrados do |t|
      t.integer :cidadeorigem
      t.integer :cidadedestino
      t.references :politica_de_quilometragem, foreign_key: true
      t.references :tenant, foreign_key: true

      t.timestamps
    end
  end
end
