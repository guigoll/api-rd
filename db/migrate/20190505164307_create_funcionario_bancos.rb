class CreateFuncionarioBancos < ActiveRecord::Migration[5.2]
  def change
    create_table :funcionario_bancos do |t|
      t.integer :agencia
      t.integer :conta
      t.integer :digito
      t.references :funcionario, foreign_key: true
      t.references :banco, foreign_key: true

      t.timestamps
    end
  end
end
