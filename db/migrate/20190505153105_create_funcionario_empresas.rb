class CreateFuncionarioEmpresas < ActiveRecord::Migration[5.2]
  def change
    create_table :funcionario_empresas do |t|
      t.references :funcionario, foreign_key: true
      t.references :empresa, foreign_key: true
      t.references :perfil, foreign_key: true

      t.timestamps
    end
  end
end
