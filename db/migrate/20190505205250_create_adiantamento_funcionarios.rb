class CreateAdiantamentoFuncionarios < ActiveRecord::Migration[5.2]
  def change
    create_table :adiantamento_funcionarios do |t|
      t.references :funcionario, foreign_key: true
      t.references :adiantamento, foreign_key: true
      t.boolean :utilizado

      t.timestamps
    end
  end
end
