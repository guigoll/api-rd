class CreatePercursos < ActiveRecord::Migration[5.2]
  def change
    create_table :percursos do |t|
      t.string :descricao
      t.boolean :reembolsavel
      t.text :observacao   
      t.references :centro_de_custo, foreign_key: true
      t.references :tipo_pagamento, foreign_key: true
      t.references :trecho_pre_cadastrado, foreign_key: true
      t.timestamps
    end
  end 
end
