class CreateEnderecos < ActiveRecord::Migration[5.2]
  def change
    create_table :enderecos do |t|
      t.string :logadouro
      t.integer :numero
      t.string :complemento
      t.string :bairo
      t.string :cep
      t.references :cidade, foreign_key: true
      t.references :pessoa, foreign_key: true

      t.timestamps
    end
  end
end
