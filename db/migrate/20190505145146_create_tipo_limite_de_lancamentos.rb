class CreateTipoLimiteDeLancamentos < ActiveRecord::Migration[5.2]
  def change
    create_table  :tipo_limite_de_lancamentos do |t|
      t.string :descricao

      t.timestamps
    end
  end
end
