class CreateEmpresas < ActiveRecord::Migration[5.2]
  def change
    create_table :empresas do |t|
      t.references :pessoa, foreign_key: true
      t.boolean :matriz
      t.references :tenant, foreign_key: true

      t.timestamps
    end
  end
end
