class CreateEmpresaConfiguracaos < ActiveRecord::Migration[5.2]
  def change
    create_table :empresa_configuracaos do |t|
      t.references :empresa, foreign_key: true
      t.boolean :permitiralteracaomembrosequipeapenasporadmin
      t.boolean :permitirenviodeemaildeaprovacaoderelatorio
      t.boolean :descricaodespesaobrigatoria
      t.boolean :tornacampoformadepagtoobrigatorio
      t.boolean :permitirlancamentoscomvalornegativo
      t.boolean :tornarcampoprojetodadespesaobrigatorio
      t.boolean :permitirutiliaradiantamento

      t.timestamps
    end
  end
end
