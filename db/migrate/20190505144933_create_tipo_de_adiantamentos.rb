class CreateTipoDeAdiantamentos < ActiveRecord::Migration[5.2]
  def change
    create_table  :tipo_de_adiantamentos do |t|
      t.string :descricao
      t.decimal :valorlimite
      t.boolean :fundofixo
      t.references :tenant, foreign_key: true

      t.timestamps
    end
  end
end
