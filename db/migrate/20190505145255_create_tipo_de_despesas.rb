class CreateTipoDeDespesas < ActiveRecord::Migration[5.2]
  def change
    create_table :tipo_de_despesas do |t|
      t.string :descricao
      t.references :tipo_limite, foreign_key: true
      t.references :tipo_limite_de_lancamento, foreign_key: true
      t.boolean :permitirultrapassarlimite
      t.boolean :observacaoobrigatorio
      t.boolean :exigerecibo
      t.references :tenant, foreign_key: true

      t.timestamps
    end
  end
end
