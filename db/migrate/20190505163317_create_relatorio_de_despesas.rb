class CreateRelatorioDeDespesas < ActiveRecord::Migration[5.2]
  def change
    create_table :relatorio_de_despesas do |t|
      t.string :descricao
      t.datetime :envioparaaprovacao
      t.references :funcionario, foreign_key: true
      t.references :fluxo_de_aprovacao, foreign_key: true     
  
      t.timestamps
    end
  end
end
