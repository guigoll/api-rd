class CreateFuncionarios < ActiveRecord::Migration[5.2]
  def change
    create_table :funcionarios do |t|
      t.references :pessoa, foreign_key: true
      t.references :centro_de_custo, foreign_key: true
      t.references :departamento, foreign_key: true
      t.string :integracao
      t.references :politica_de_despesa, foreign_key: true
      t.references :fluxo_de_aprovacao, foreign_key: true
      t.references :politica_de_quilometragem, foreign_key: true

      t.timestamps 
    end
  end
end
