class CreateProjetos < ActiveRecord::Migration[5.2]
  def change
    create_table :projetos do |t|
      t.string :descricao, foreign_key: true
      t.references :cliente, foreign_key: true
      t.references :tenant, foreign_key: true

      t.timestamps
    end
  end
end
