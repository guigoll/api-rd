class CreateDespesas < ActiveRecord::Migration[5.2]
  def change
    create_table :despesas do |t|
      t.string :descricao
      t.references :moeda, foreign_key: true
      t.references :centro_de_custo, foreign_key: true
      t.references :tipo_pagamento, foreign_key: true
      t.references :tipo_de_despesa, foreign_key: true
      t.text :observacao
      t.boolean :reembolsavel
      t.references :politica_de_despesa, foreign_key: true
      t.references :funcionario, foreign_key: true

      t.timestamps
    end
  end
end
