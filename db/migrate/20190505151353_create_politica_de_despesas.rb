class CreatePoliticaDeDespesas < ActiveRecord::Migration[5.2]
  def change
    create_table  :politica_de_despesas do |t|
      t.string :descricao
      t.integer :limitemaximodediaspassados
      t.references :tenant, foreign_key: true

      t.timestamps
    end
  end
end
