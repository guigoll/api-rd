class CreateAdiantamentos < ActiveRecord::Migration[5.2]
  def change
    create_table :adiantamentos do |t|
      t.references :funcionario, foreign_key: true
      t.datetime :datanecessidade
      t.string :finalidade
      t.decimal :valor
      t.text :observacao
      t.references :moeda, foreign_key: true
      t.references :tipo_de_adiantamento, foreign_key: true

      t.timestamps
    end
  end
end
