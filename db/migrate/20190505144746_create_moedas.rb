class CreateMoedas < ActiveRecord::Migration[5.2]
  def change
    create_table  :moedas do |t|
      t.string :descricao
      t.references :tenant, foreign_key: true

      t.timestamps
    end
  end
end
