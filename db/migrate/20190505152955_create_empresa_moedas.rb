class CreateEmpresaMoedas < ActiveRecord::Migration[5.2]
  def change
    create_table :empresa_moedas do |t|
      t.references :empresa, foreign_key: true
      t.references :moeda, foreign_key: true

      t.timestamps
    end
  end
end
