class CreateCartaoOperadoras < ActiveRecord::Migration[5.2]
  def change
    create_table  :cartao_operadoras do |t|
      t.string :descricao

      t.timestamps
    end
  end
end
