class CreateTipoPagamentos < ActiveRecord::Migration[5.2]
  def change
    create_table :tipo_pagamentos do |t|
      t.string :descricao
      t.boolean :reembolsavel
      t.boolean :baixardoadiantamento
      t.references :moeda, foreign_key: true
      t.references :tenant, foreign_key: true

      t.timestamps
    end
  end
end
