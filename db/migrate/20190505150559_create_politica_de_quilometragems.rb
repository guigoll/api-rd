class CreatePoliticaDeQuilometragems < ActiveRecord::Migration[5.2]
  def change
    create_table :politica_de_quilometragems do |t|
      t.decimal :valorpagoporkm
      t.boolean :permitiralterarvalormanualmente
      t.references :tenant, foreign_key: true

      t.timestamps
    end
  end
end
