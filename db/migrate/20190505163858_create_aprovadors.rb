class CreateAprovadors < ActiveRecord::Migration[5.2]
  def change
    create_table :aprovadors do |t|
      t.string :condicao
      t.integer :nivel 
      t.string :grupo
      t.references :funcionario, foreign_key: true
      t.references :fluxo_de_aprovacao, foreign_key: true

      t.timestamps
    end
  end
end
