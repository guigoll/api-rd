class CreateRelatorioDespesas < ActiveRecord::Migration[5.2]
  def change
    create_table :relatorio_despesas do |t|
      t.references :relatorio_de_despesa, foreign_key: true
      t.references :percurso, foreign_key: false
      t.references :despesa, foreign_key: false
      t.references :adiantamento, foreign_key: false

      t.timestamps
    end
  end
end
