class CreateProjetoFuncionarios < ActiveRecord::Migration[5.2]
  def change
    create_table :projeto_funcionarios do |t|
      t.references :projeto, foreign_key: true
      t.references :funcionario, foreign_key: true

      t.timestamps
    end
  end
end
