class CreateCentroDeCustos < ActiveRecord::Migration[5.2]
  def change
    create_table :centro_de_custos do |t|
      t.string :descricao
      t.string :integracao
      t.references :tenant, foreign_key: true

      t.timestamps
    end
  end
end
