class CreateClientes < ActiveRecord::Migration[5.2]
  def change
    create_table :clientes do |t|
      t.references :pessoa, foreign_key: true

      t.timestamps
    end
  end
end
