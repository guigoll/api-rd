# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_04_182459) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "adiantamento_funcionarios", force: :cascade do |t|
    t.bigint "funcionario_id"
    t.bigint "adiantamento_id"
    t.boolean "utilizado"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["adiantamento_id"], name: "index_adiantamento_funcionarios_on_adiantamento_id"
    t.index ["funcionario_id"], name: "index_adiantamento_funcionarios_on_funcionario_id"
  end

  create_table "adiantamentos", force: :cascade do |t|
    t.bigint "funcionario_id"
    t.datetime "datanecessidade"
    t.string "finalidade"
    t.decimal "valor"
    t.text "observacao"
    t.bigint "moeda_id"
    t.bigint "tipo_de_adiantamento_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["funcionario_id"], name: "index_adiantamentos_on_funcionario_id"
    t.index ["moeda_id"], name: "index_adiantamentos_on_moeda_id"
    t.index ["tipo_de_adiantamento_id"], name: "index_adiantamentos_on_tipo_de_adiantamento_id"
  end

  create_table "aprovadors", force: :cascade do |t|
    t.string "condicao"
    t.integer "nivel"
    t.string "grupo"
    t.bigint "funcionario_id"
    t.bigint "fluxo_de_aprovacao_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["fluxo_de_aprovacao_id"], name: "index_aprovadors_on_fluxo_de_aprovacao_id"
    t.index ["funcionario_id"], name: "index_aprovadors_on_funcionario_id"
  end

  create_table "bancos", force: :cascade do |t|
    t.string "descricao"
    t.integer "codigobc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cartao_operadoras", force: :cascade do |t|
    t.string "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "centro_de_custos", force: :cascade do |t|
    t.string "descricao"
    t.string "integracao"
    t.bigint "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tenant_id"], name: "index_centro_de_custos_on_tenant_id"
  end

  create_table "cidades", force: :cascade do |t|
    t.string "descricao"
    t.bigint "estado_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["estado_id"], name: "index_cidades_on_estado_id"
  end

  create_table "clientes", force: :cascade do |t|
    t.bigint "pessoa_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pessoa_id"], name: "index_clientes_on_pessoa_id"
  end

  create_table "departamentos", force: :cascade do |t|
    t.string "descricao"
    t.string "integracao"
    t.bigint "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tenant_id"], name: "index_departamentos_on_tenant_id"
  end

  create_table "despesas", force: :cascade do |t|
    t.string "descricao"
    t.bigint "moeda_id"
    t.bigint "centro_de_custo_id"
    t.bigint "tipo_pagamento_id"
    t.bigint "tipo_de_despesa_id"
    t.text "observacao"
    t.boolean "reembolsavel"
    t.bigint "politica_de_despesa_id"
    t.bigint "funcionario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["centro_de_custo_id"], name: "index_despesas_on_centro_de_custo_id"
    t.index ["funcionario_id"], name: "index_despesas_on_funcionario_id"
    t.index ["moeda_id"], name: "index_despesas_on_moeda_id"
    t.index ["politica_de_despesa_id"], name: "index_despesas_on_politica_de_despesa_id"
    t.index ["tipo_de_despesa_id"], name: "index_despesas_on_tipo_de_despesa_id"
    t.index ["tipo_pagamento_id"], name: "index_despesas_on_tipo_pagamento_id"
  end

  create_table "empresa_configuracaos", force: :cascade do |t|
    t.bigint "empresa_id"
    t.boolean "permitiralteracaomembrosequipeapenasporadmin"
    t.boolean "permitirenviodeemaildeaprovacaoderelatorio"
    t.boolean "descricaodespesaobrigatoria"
    t.boolean "tornacampoformadepagtoobrigatorio"
    t.boolean "permitirlancamentoscomvalornegativo"
    t.boolean "tornarcampoprojetodadespesaobrigatorio"
    t.boolean "permitirutiliaradiantamento"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["empresa_id"], name: "index_empresa_configuracaos_on_empresa_id"
  end

  create_table "empresa_moedas", force: :cascade do |t|
    t.bigint "empresa_id"
    t.bigint "moeda_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["empresa_id"], name: "index_empresa_moedas_on_empresa_id"
    t.index ["moeda_id"], name: "index_empresa_moedas_on_moeda_id"
  end

  create_table "empresas", force: :cascade do |t|
    t.bigint "pessoa_id"
    t.boolean "matriz"
    t.bigint "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pessoa_id"], name: "index_empresas_on_pessoa_id"
    t.index ["tenant_id"], name: "index_empresas_on_tenant_id"
  end

  create_table "enderecos", force: :cascade do |t|
    t.string "logadouro"
    t.integer "numero"
    t.string "complemento"
    t.string "bairo"
    t.string "cep"
    t.bigint "cidade_id"
    t.bigint "pessoa_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cidade_id"], name: "index_enderecos_on_cidade_id"
    t.index ["pessoa_id"], name: "index_enderecos_on_pessoa_id"
  end

  create_table "estados", force: :cascade do |t|
    t.string "descricao"
    t.string "sigla"
    t.bigint "pai_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pai_id"], name: "index_estados_on_pai_id"
  end

  create_table "fluxo_de_aprovacaos", force: :cascade do |t|
    t.string "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "funcionario_bancos", force: :cascade do |t|
    t.integer "agencia"
    t.integer "conta"
    t.integer "digito"
    t.bigint "funcionario_id"
    t.bigint "banco_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["banco_id"], name: "index_funcionario_bancos_on_banco_id"
    t.index ["funcionario_id"], name: "index_funcionario_bancos_on_funcionario_id"
  end

  create_table "funcionario_cartaos", force: :cascade do |t|
    t.bigint "cartao_operadora_id"
    t.integer "numero"
    t.integer "digito"
    t.boolean "ativo"
    t.decimal "limite"
    t.bigint "funcionario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cartao_operadora_id"], name: "index_funcionario_cartaos_on_cartao_operadora_id"
    t.index ["funcionario_id"], name: "index_funcionario_cartaos_on_funcionario_id"
  end

  create_table "funcionario_empresas", force: :cascade do |t|
    t.bigint "funcionario_id"
    t.bigint "empresa_id"
    t.bigint "perfil_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["empresa_id"], name: "index_funcionario_empresas_on_empresa_id"
    t.index ["funcionario_id"], name: "index_funcionario_empresas_on_funcionario_id"
    t.index ["perfil_id"], name: "index_funcionario_empresas_on_perfil_id"
  end

  create_table "funcionarios", force: :cascade do |t|
    t.bigint "pessoa_id"
    t.bigint "centro_de_custo_id"
    t.bigint "departamento_id"
    t.string "integracao"
    t.bigint "politica_de_despesa_id"
    t.bigint "fluxo_de_aprovacao_id"
    t.bigint "politica_de_quilometragem_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["centro_de_custo_id"], name: "index_funcionarios_on_centro_de_custo_id"
    t.index ["departamento_id"], name: "index_funcionarios_on_departamento_id"
    t.index ["fluxo_de_aprovacao_id"], name: "index_funcionarios_on_fluxo_de_aprovacao_id"
    t.index ["pessoa_id"], name: "index_funcionarios_on_pessoa_id"
    t.index ["politica_de_despesa_id"], name: "index_funcionarios_on_politica_de_despesa_id"
    t.index ["politica_de_quilometragem_id"], name: "index_funcionarios_on_politica_de_quilometragem_id"
  end

  create_table "moedas", force: :cascade do |t|
    t.string "descricao"
    t.bigint "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tenant_id"], name: "index_moedas_on_tenant_id"
  end

  create_table "pais", force: :cascade do |t|
    t.string "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "percursos", force: :cascade do |t|
    t.string "descricao"
    t.boolean "reembolsavel"
    t.text "observacao"
    t.bigint "centro_de_custo_id"
    t.bigint "tipo_pagamento_id"
    t.bigint "trecho_pre_cadastrado_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["centro_de_custo_id"], name: "index_percursos_on_centro_de_custo_id"
    t.index ["tipo_pagamento_id"], name: "index_percursos_on_tipo_pagamento_id"
    t.index ["trecho_pre_cadastrado_id"], name: "index_percursos_on_trecho_pre_cadastrado_id"
  end

  create_table "perfils", force: :cascade do |t|
    t.string "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pessoas", force: :cascade do |t|
    t.bigint "tipo_pessoa_id"
    t.bigint "tenant_id"
    t.boolean "ativo"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tenant_id"], name: "index_pessoas_on_tenant_id"
    t.index ["tipo_pessoa_id"], name: "index_pessoas_on_tipo_pessoa_id"
  end

  create_table "politica_de_despesas", force: :cascade do |t|
    t.string "descricao"
    t.integer "limitemaximodediaspassados"
    t.bigint "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tenant_id"], name: "index_politica_de_despesas_on_tenant_id"
  end

  create_table "politica_de_quilometragems", force: :cascade do |t|
    t.decimal "valorpagoporkm"
    t.boolean "permitiralterarvalormanualmente"
    t.bigint "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tenant_id"], name: "index_politica_de_quilometragems_on_tenant_id"
  end

  create_table "projeto_funcionarios", force: :cascade do |t|
    t.bigint "projeto_id"
    t.bigint "funcionario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["funcionario_id"], name: "index_projeto_funcionarios_on_funcionario_id"
    t.index ["projeto_id"], name: "index_projeto_funcionarios_on_projeto_id"
  end

  create_table "projetos", force: :cascade do |t|
    t.string "descricao"
    t.bigint "cliente_id"
    t.bigint "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cliente_id"], name: "index_projetos_on_cliente_id"
    t.index ["tenant_id"], name: "index_projetos_on_tenant_id"
  end

  create_table "quilometragems", force: :cascade do |t|
    t.string "descricao"
    t.decimal "valorpagoporkm"
    t.boolean "permitiralterarvalormanualmente"
    t.boolean "exigirimagem"
    t.bigint "politica_de_quilometragem_id"
    t.bigint "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["politica_de_quilometragem_id"], name: "index_quilometragems_on_politica_de_quilometragem_id"
    t.index ["tenant_id"], name: "index_quilometragems_on_tenant_id"
  end

  create_table "relatorio_de_despesas", force: :cascade do |t|
    t.string "descricao"
    t.datetime "envioparaaprovacao"
    t.bigint "funcionario_id"
    t.bigint "fluxo_de_aprovacao_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["fluxo_de_aprovacao_id"], name: "index_relatorio_de_despesas_on_fluxo_de_aprovacao_id"
    t.index ["funcionario_id"], name: "index_relatorio_de_despesas_on_funcionario_id"
  end

  create_table "relatorio_despesas", force: :cascade do |t|
    t.bigint "relatorio_de_despesa_id"
    t.bigint "percurso_id"
    t.bigint "despesa_id"
    t.bigint "adiantamento_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["adiantamento_id"], name: "index_relatorio_despesas_on_adiantamento_id"
    t.index ["despesa_id"], name: "index_relatorio_despesas_on_despesa_id"
    t.index ["percurso_id"], name: "index_relatorio_despesas_on_percurso_id"
    t.index ["relatorio_de_despesa_id"], name: "index_relatorio_despesas_on_relatorio_de_despesa_id"
  end

  create_table "telefones", force: :cascade do |t|
    t.string "numero"
    t.bigint "pessoa_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pessoa_id"], name: "index_telefones_on_pessoa_id"
  end

  create_table "tenants", force: :cascade do |t|
    t.integer "codigocliente"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipo_de_adiantamentos", force: :cascade do |t|
    t.string "descricao"
    t.decimal "valorlimite"
    t.boolean "fundofixo"
    t.bigint "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tenant_id"], name: "index_tipo_de_adiantamentos_on_tenant_id"
  end

  create_table "tipo_de_despesas", force: :cascade do |t|
    t.string "descricao"
    t.bigint "tipo_limite_id"
    t.bigint "tipo_limite_de_lancamento_id"
    t.boolean "permitirultrapassarlimite"
    t.boolean "observacaoobrigatorio"
    t.boolean "exigerecibo"
    t.bigint "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tenant_id"], name: "index_tipo_de_despesas_on_tenant_id"
    t.index ["tipo_limite_de_lancamento_id"], name: "index_tipo_de_despesas_on_tipo_limite_de_lancamento_id"
    t.index ["tipo_limite_id"], name: "index_tipo_de_despesas_on_tipo_limite_id"
  end

  create_table "tipo_limite_de_lancamentos", force: :cascade do |t|
    t.string "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipo_limites", force: :cascade do |t|
    t.string "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipo_pagamentos", force: :cascade do |t|
    t.string "descricao"
    t.boolean "reembolsavel"
    t.boolean "baixardoadiantamento"
    t.bigint "moeda_id"
    t.bigint "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["moeda_id"], name: "index_tipo_pagamentos_on_moeda_id"
    t.index ["tenant_id"], name: "index_tipo_pagamentos_on_tenant_id"
  end

  create_table "tipo_pessoas", force: :cascade do |t|
    t.string "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "trecho_pre_cadastrados", force: :cascade do |t|
    t.integer "cidadeorigem"
    t.integer "cidadedestino"
    t.bigint "politica_de_quilometragem_id"
    t.bigint "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["politica_de_quilometragem_id"], name: "index_trecho_pre_cadastrados_on_politica_de_quilometragem_id"
    t.index ["tenant_id"], name: "index_trecho_pre_cadastrados_on_tenant_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "auth_token"
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.json "tokens"
    t.bigint "tenant_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["tenant_id"], name: "index_users_on_tenant_id"
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  add_foreign_key "adiantamento_funcionarios", "adiantamentos"
  add_foreign_key "adiantamento_funcionarios", "funcionarios"
  add_foreign_key "adiantamentos", "funcionarios"
  add_foreign_key "adiantamentos", "moedas"
  add_foreign_key "adiantamentos", "tipo_de_adiantamentos"
  add_foreign_key "aprovadors", "fluxo_de_aprovacaos"
  add_foreign_key "aprovadors", "funcionarios"
  add_foreign_key "centro_de_custos", "tenants"
  add_foreign_key "cidades", "estados"
  add_foreign_key "clientes", "pessoas"
  add_foreign_key "departamentos", "tenants"
  add_foreign_key "despesas", "centro_de_custos"
  add_foreign_key "despesas", "funcionarios"
  add_foreign_key "despesas", "moedas"
  add_foreign_key "despesas", "politica_de_despesas"
  add_foreign_key "despesas", "tipo_de_despesas"
  add_foreign_key "despesas", "tipo_pagamentos"
  add_foreign_key "empresa_configuracaos", "empresas"
  add_foreign_key "empresa_moedas", "empresas"
  add_foreign_key "empresa_moedas", "moedas"
  add_foreign_key "empresas", "pessoas"
  add_foreign_key "empresas", "tenants"
  add_foreign_key "enderecos", "cidades"
  add_foreign_key "enderecos", "pessoas"
  add_foreign_key "estados", "pais"
  add_foreign_key "funcionario_bancos", "bancos"
  add_foreign_key "funcionario_bancos", "funcionarios"
  add_foreign_key "funcionario_cartaos", "cartao_operadoras"
  add_foreign_key "funcionario_cartaos", "funcionarios"
  add_foreign_key "funcionario_empresas", "empresas"
  add_foreign_key "funcionario_empresas", "funcionarios"
  add_foreign_key "funcionario_empresas", "perfils"
  add_foreign_key "funcionarios", "centro_de_custos"
  add_foreign_key "funcionarios", "departamentos"
  add_foreign_key "funcionarios", "fluxo_de_aprovacaos"
  add_foreign_key "funcionarios", "pessoas"
  add_foreign_key "funcionarios", "politica_de_despesas"
  add_foreign_key "funcionarios", "politica_de_quilometragems"
  add_foreign_key "moedas", "tenants"
  add_foreign_key "percursos", "centro_de_custos"
  add_foreign_key "percursos", "tipo_pagamentos"
  add_foreign_key "percursos", "trecho_pre_cadastrados"
  add_foreign_key "pessoas", "tenants"
  add_foreign_key "pessoas", "tipo_pessoas"
  add_foreign_key "politica_de_despesas", "tenants"
  add_foreign_key "politica_de_quilometragems", "tenants"
  add_foreign_key "projeto_funcionarios", "funcionarios"
  add_foreign_key "projeto_funcionarios", "projetos"
  add_foreign_key "projetos", "clientes"
  add_foreign_key "projetos", "tenants"
  add_foreign_key "quilometragems", "tenants"
  add_foreign_key "relatorio_de_despesas", "fluxo_de_aprovacaos"
  add_foreign_key "relatorio_de_despesas", "funcionarios"
  add_foreign_key "relatorio_despesas", "relatorio_de_despesas"
  add_foreign_key "telefones", "pessoas"
  add_foreign_key "tipo_de_adiantamentos", "tenants"
  add_foreign_key "tipo_de_despesas", "tenants"
  add_foreign_key "tipo_de_despesas", "tipo_limite_de_lancamentos"
  add_foreign_key "tipo_de_despesas", "tipo_limites"
  add_foreign_key "tipo_pagamentos", "moedas"
  add_foreign_key "tipo_pagamentos", "tenants"
  add_foreign_key "trecho_pre_cadastrados", "politica_de_quilometragems"
  add_foreign_key "trecho_pre_cadastrados", "tenants"
  add_foreign_key "users", "tenants"
end
